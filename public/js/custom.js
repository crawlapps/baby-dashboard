var crawlapps_baby_dashboard1 = {
    init: function () {
        this.imagepreview();
    },
    imagepreview: function(){
        $(document).on('change','.uploadFile',function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                var id = $( this ).attr('id');
                var file_div = $(this);
                reader.onload = function (e) {
                    if( id == "gridFile" ){
                        $('#grid_preview').css('display','block');
                        $('#grid_image').attr('src', e.target.result);
                    }else{
                        console.log($(this).parent('.prw-img'));
                        file_div.parent('.prw-img').find('img').attr('src',e.target.result);
                    }
                }
                reader.readAsDataURL(this.files[0]);
            }
            $('.elements').css('display', 'block');
        });

        $(document).on('change','.uploadMultipleFile',function(){
            $('.prw-images').html('');
            imagesPreview(this, 'div.prw-images');
        });

        var imagesPreview = function(input, placeToInsertImagePreview) {
            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    var sng_class = "single-img"+ i;
                    reader.onload = function(event) {
                        $($.parseHTML('<img height="200px" width="200px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }

        }

    }
}
$(document).ready(function () {
    crawlapps_baby_dashboard1.init();
});
