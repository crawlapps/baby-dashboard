<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\LineItems;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class OrderController extends Controller
{
    private $grandTotal = 0;
    public function index(Request $request){
        try{
            $user = Auth::user();
            if( $request->d != '' ) {
                $entities = Order::where('user_id', $user->id)->where('date', $request->d)->orderBy('created_at', 'desc')->paginate(10);
            }else{
                $entities = Order::where('user_id', $user->id)->orderBy('created_at', 'desc')->paginate(10);
            }

            if( $entities ){
                $entity = $entities->map(function ($name) use ($user) {
                    $this->grandTotal += $name->order_total;
                    return [
                        'order_id' => $name->order_id,
                        'order_number' => $name->order_number,
                        'order_name' => $name->order_name,
                        'order_total' => $name->order_total,
                        'order_counter' => $name->order_counter,
                        'date' => $name->date,
                        'order_url' => 'https://' . $user->name . '/admin/orders/' . $name->order_id,
                    ];
                })->toArray();
            }
            $data['data'] = $entity;
            $data['grandTotal'] = number_format($this->grandTotal, 2);
            $data['prev'] = ( $entities->previousPageUrl() ) ? $entities->previousPageUrl() : '';
            $data['next'] = ( $entities->nextPageUrl() ) ? $entities->nextPageUrl() : '';
            return response::json(['data' => $data ], 200);
        }catch(\Exception $e){
            return response::json(['data' => $e ], 422);
        }
    }

    public function lineItems(Request $request){
        try{
            $user = Auth::user();
            $entities = LineItems::select('product_type')->where('date', $request->d)->groupBy('product_type')->paginate(10);

            $product_types = $entities->map(function ($type) {
                return $type->product_type;
            })->toArray();

            $final_entity = [];
            foreach ( $product_types as $key=>$val  ){
                $fnt = [];
                $entity = LineItems::where('date', $request->d)->where('product_type', $val)->get();

                if( $entity->count() > 0 ){
                    $fnt[] = $val;
                }
                $fn = $entity->map(function ($name) {
                    return $name;
                })->toArray();

                $fn = array_merge($fnt, $fn);
                $final_entity = array_merge($final_entity, $fn);
            }
//            $entities = LineItems::where('date', $request->d)->groupBy('product_type')->havingRaw('product_type' , 'in' , $product_types)->paginate(10);

//            dd($entities);
//            if( $entities ){
//                $entity = $entities->map(function ($name) {
//                    $this->grandTotal += $name->total;
//                    return $name;
//                });
//            }
            $data['data'] = $final_entity;
            $data['grandTotal'] = $this->grandTotal;
            $data['prev'] = ( $entities->previousPageUrl() ) ? $entities->previousPageUrl() : '';
            $data['next'] = ( $entities->nextPageUrl() ) ? $entities->nextPageUrl() : '';
            return response::json(['data' => $data ], 200);
        }catch(\Exception $e){
            return response::json(['data' => $e->getMessage() ], 422);
        }
    }
}
