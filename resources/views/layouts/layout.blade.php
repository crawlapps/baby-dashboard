<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baby</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('distribution/vendor/bootstrap/css/bootstrap.css') }}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href={{ asset("distribution/css/orionicons.css") }}>
    <!-- theme stylesheet-->
    <link rel="stylesheet" href={{ asset("distribution/css/style.default.css") }} id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href={{ asset("distribution/css/custom.css")  }}>
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset("distribution/img/favicon.png?3") }}">
     <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
    <link href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css" rel="stylesheet" type="text/css" />
{{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">--}}
    <!-- Tweaks for older IEs--><!--[if lt IE 9]><![endif]-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="{{ asset("distribution/vendor/jquery/jquery.min.js") }}"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <style>
        .prw-images > img {
            background-color: #5d5959;
        }
    </style>
</head>
<body>
<!-- navbar-->
<header class="header">
    <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="{{route('relation.index')}}" class="navbar-brand font-weight-bold text-uppercase text-base">Baby</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">

            <li class="nav-item dropdown ml-auto">
                <form method="post" action="{{route('logout')}}">
                    @csrf
                    <button class="fa fa-sign-out fa-lg" style="border-radius: 3px;"></button>
                </form>

                {{--                <div aria-labelledby="userInfo" class="dropdown-menu">--}}
                {{--                  --}}
                {{--                        <div class="dropdown-divider"></div><button class="dropdown-item"><img src="{{asset('distribution/img/shutdown.png')}}"></button>--}}
                {{--                    --}}
                {{--                </div>--}}
            </li>
        </ul>
    </nav>
</header>
<div class="d-flex align-items-stretch">
    <div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
                        <li class="sidebar-list-item"><a href="{{ route('homescreen.index')  }}" class="sidebar-link text-muted {{ Request::is('homescreen')  ? 'active' : '' }}"><i class="o-home-1 mr-3 text-gray"></i><span>Home Screen</span></a></li>

            <li class="sidebar-list-item"><a href="{{route('singlecategory.index')}}" class="sidebar-link text-muted  {{ Request::is('singlecategory')  ? 'active' : '' }}"><i class="o-imac-screen-1 mr-3 text-gray"></i><span>Single</span></a></li>

            <li class="sidebar-list-item"><a href="{{ route('relationtype.index')  }}" class="sidebar-link text-muted  {{ Request::is('relationtype')  ? 'active' : '' }}"><i class="o-contact-card-1 mr-3 text-gray"></i><span>Relation</span></a></li>

            <li class="sidebar-list-item"><a href="{{route('collagehomescreen')}}" class="sidebar-link text-muted  {{ Request::is('collagehomescreen')  ? 'active' : '' }}"><i class="o-table-content-1 mr-3 text-gray"></i><span>Collage</span></a></li>

{{--            <li class="sidebar-list-item"><a href="{{route('collagecategory.index')}}" class="sidebar-link text-muted  {{ Request::is('collagecategory')  ? 'active' : '' }}"><i class="o-table-content-1 mr-3 text-gray"></i><span>Collage</span></a></li>--}}

            <li class="sidebar-list-item"><a href="{{ route('milestone')  }}" class="sidebar-link text-muted  {{ Request::is('milestone')  ? 'active' : '' }}"><i class="o-earth-globe-1 mr-3 text-gray"></i><span>Baby Milestone</span></a></li>

            <li class="sidebar-list-item"><a href="{{ route('category.index')  }}" class="sidebar-link text-muted  {{ Request::is('category')  ? 'active' : '' }}"><i class="o-stack-1 mr-3 text-gray"></i><span>Sticker</span></a></li>

            <li class="sidebar-list-item"><a href="{{ route('backgroundcategory.index')  }}" class="sidebar-link text-muted  {{ Request::is('backgroundcategory*') ? 'active' : '' }}"><i class="o-paperwork-1 mr-3 text-gray"></i><span>Background</span></a></li>

            <li class="sidebar-list-item"><a href="{{ route('font.index')  }}" class="sidebar-link text-muted  {{ Request::is('font*') ? 'active' : '' }}"><i class="o-document-1 mr-3 text-gray"></i><span>Font</span></a></li>

            <li class="sidebar-list-item"><a href="{{ route('setting.index')  }}" class="sidebar-link text-muted  {{ Request::is('setting*') ? 'active' : '' }}"><i class="o-diploma-1 mr-3 text-gray"></i><span>Setting</span></a></li>
        </ul>

    </div>
    <div class="page-holder w-100 d-flex flex-wrap">

        @yield('content')

    </div><div></div>

</div>
<!-- JavaScript files-->

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"> </script>
<script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
<script src="{{ asset("distribution/vendor/popper.js/umd/popper.min.js")}}"> </script>
<script src="{{ asset("distribution/vendor/bootstrap/js/bootstrap.js") }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"> </script>
{{--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"> </script>--}}
<script src="{{ asset("distribution/vendor/jquery.cookie/jquery.cookie.js") }}"> </script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script src="{{ asset("distribution/js/front.js") }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<script src="{{ asset("js/custom.js")}}"> </script>
</body>
</html>
