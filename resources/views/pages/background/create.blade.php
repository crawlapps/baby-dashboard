@extends('layouts.layout')
@section('content')
    <div class="container py-5" style="padding:10px;">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Background</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        <form method="post" action="{{($background) ? route('background.update',$background->id) : route('background.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if($background)
                                @method('patch')
                            @endif
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Background Category</label>
                                <input type="hidden" name="category" value="{{$category->id}}">
                                <input type="text" name="category_name" value="{{$category->name}}" class="form-control" readonly>
                                @if ($errors->has('category'))
                                    <span class="text-danger">{{ $errors->first('category') }}</span>
                                @endif
                            </div>

                            <div class="form-group prw-img">
                                <label class="form-control-label text-uppercase">Image</label>
                                <div id="" class="prw-images">
                                    <img src="{{($background) ? url('storage/uploads/background/') . '/'. $background->file_background : ''}}"  width="50px">
                                </div>

                                <input type="file" placeholder="Background Image" name="file_background" class="form-control uploadFile" value="">

                                @if ($errors->has('file_background'))
                                    <span class="text-danger">{{ $errors->first('file_background') }}</span>
                                @endif
                                @if ($errors->has('file_background.*'))
                                    <span class="text-danger">{{ $errors->first('file_background.*') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
