@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-9">
                <h2>Collage Type</h2>
            </div>
            <div class="col-md-3">
                <a class="btn btn-lg form-control add-btn pull-right" href="{{ route('collagecategory.create') }}">Create Category</a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="collage-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Is_Active</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
                this.collagecategory_delete();
                this.change_status();
            },
            datatableInit: function(){
                var ajax_url = '';
                ajax_url = "{{ route('collagecategory.index')}}?api=1";
                var i=1;
                $collage_table = $('#collage-table').DataTable({
                    "serverSide": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "id"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return i++;
                            },
                            "targets": 0,
                        },
                        {
                            "render": function (data, type, row) {
                                let r1 = "{{url('collagehomescreen?id=')}}" + row.id;
                                return `<a href="` + r1 + `">`+ row.name + `</a>`;
                            },
                            "targets": 1,
                        },
                        {
                            "render": function (data, type, row) {
                                var check = (row.is_active == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap status chng-status" name="status" id="status" value="1"` + check + ` data-value="` + row.is_active + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;

                            },
                            "targets": 2,
                        },
                        {
                            "render": function (data, type, row) {
                                let r1 = "{{route('collagecategory.edit',"__")}}"
                                r1 = r1.replace("__", row.id);
                                return  `<a href="`+r1+`" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger del-collagecategory" data-id="`+row.id+`"  type="submit"><i class="fa fa-trash-o"></i></button>`
                            },
                            "targets": 3,
                        },
                    ],
                    "pageLength": 50,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: true,
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function() {
                        i=0;
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });
                $collage_table.on( 'row-reorder', function ( e, details, edit ) {

                    var data = [];
                    for ( var i=0; i < details.length ; i++ ) {
                        data[details[i].newPosition] = details[i].node.id;
                    }
                    data = {
                        'items' : data,
                        "_token": "{{ csrf_token() }}",
                    };
                    $.ajax({
                        type: 'POST',
                        url:  "{{ route('collagecategory.categoryreorder')  }}",
                        data: data,
                        success: function (data) {
                            // $collage_table.ajax.reload();
                        }
                    })
                });
            },
            collagecategory_delete: function () {
                $(document).on('click', '.del-collagecategory', function () {
                    var id = $(this).data('id');
                    swal({
                            title: "Are you sure?",
                            text: "To delete this category...",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            showCancelButton: true,
                            cancelButtonText: 'Cancel',
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            let url = "{{route('collagecategory.destroy',"__")}}";
                            url = url.replace("__", id);
                            $.ajax({
                                url: url,
                                type: "post",
                                data: {'_method': 'delete', '_token': "{{ csrf_token() }}"},
                                success: function (data) {
                                    swal({
                                        icon: "success",
                                    });
                                    if (data['status'] == "1") {
                                        window.location.reload();
                                    }
                                },
                            });
                        });
                });
            },
            change_status: function () {
                $(document).on('change', '.chng-status', function () {
                    var value = $(this).data('value');
                    var id = $(this).data('id');
                    let url = "{{url('collagecategory/change_status',"__")}}";
                    url = url.replace("__", id);
                    $.ajax({
                        url: url,
                        type: "post",
                        data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'data': value},
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            },
        };

        $(document).ready(function(){
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
