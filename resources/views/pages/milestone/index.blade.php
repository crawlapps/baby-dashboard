@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-10">
                <h2>Poster</h2>
            </div>

            <div class="col-md-12">
                <table class="table table-striped" id="milestone-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>MileStone</th>
                    </tr>
                    </thead>
                    <tbody>
                        @for( $i=1; $i<=12; $i++ )
                            <tr>
                            <td>{{$i}}</td>
                            <td><a href="{{url('poster').'?month='.$i}}" class="btn btn-info">{{$i}} Month Completed</a></td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
