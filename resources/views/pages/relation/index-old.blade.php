@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-10">
                <h2>{{$relation_name}}</h2>
            </div>
            <div class="col-md-2">
                <a class="btn btn-lg form-control add-btn pull-right" href="{{ route('relation.create').'?relation_id='.$relation_id }}">Add Image</a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="relation-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Image</th>
                        <th>Is_Enable</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
                this.relation_delete();
                this.change_status();
            },
            datatableInit: function () {
                var ajax_url = '';
                var id = this.getQueryStringValue("id");

                ajax_url = "{{ route('relation.index')}}?api=1&id="+id;
                var i = 1;
                $relation_table = $('#relation-table').DataTable({
                    "serverSide": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "id"},
                        {"data": "image"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return i++;
                            },
                            "targets": 0,
                        },
                        {
                            "render": function (data, type, row) {
                                let url = "{{ url('storage/uploads/relation/').'/'}}" + row.image;
                                return `<img height="50" width="50" src="` + url + `" >`;
                            },
                            "targets": 1,
                        },
                        {
                            "render": function (data, type, row) {
                                var check = (row.is_enable == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap status chng-status" name="status" id="status" value="1"` + check + ` data-value="` + row.is_enable + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;

                            },
                            "targets": 2,
                        },
                        {
                            "render": function (data, type, row) {
                                let r1 = "{{route('relation.edit',"__")}}"
                                let r3 = "{{route('relation.destroy',"__")}}";
                                r1 = r1.replace("__", row.id);
                                r3 = r3.replace("__", row.id);
                                return `<a href="` + r1 + `" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger del-relation" data-id="` + row.id + `"  type="submit"><i class="fa fa-trash-o"></i></button>`
                            },
                            "targets": 3,
                        },
                    ],
                    "pageLength": 10,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: true,
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function () {
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });
            },
            relation_delete: function () {
                $(document).on('click', '.del-relation', function () {
                    var id = $(this).data('id');
                    swal({
                            title: "Are you sure?",
                            text: "To delete this Relation...",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            showCancelButton: true,
                            cancelButtonText: 'Cancel',
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            let url = "{{route('relation.destroy',"__")}}";
                            url = url.replace("__", id);
                            $.ajax({
                                url: url,
                                type: "post",
                                data: {'_method': 'delete', '_token': "{{ csrf_token() }}"},
                                success: function (data) {
                                    swal({
                                        icon: "success",
                                    });
                                    if (data['status'] == "1") {
                                        window.location.reload();
                                    }
                                },
                            });
                        });
                });
            },
            getQueryStringValue: function (key) {
                return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
            },
            change_status: function () {
                $(document).on('change', '.chng-status', function () {
                    var value = $(this).data('value');
                    var id = $(this).data('id');
                    let url = "{{url('relation/change_status',"__")}}";
                    url = url.replace("__", id);
                    $.ajax({
                        url: url,
                        type: "post",
                        data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'data': value},
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            },
        };

        $(document).ready(function () {
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
