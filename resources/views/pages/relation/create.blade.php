@extends('layouts.layout')
@section('content')
    <div class="container py-5" style="padding:10px;">

        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Relation</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        <form method="post" action="{{($relation) ? route('relation.update',$relation->id) : route('relation.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if($relation)
                                @method('patch')
                            @endif
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Name</label>
                                <input type="hidden" name="relation_type_id" value="{{$relationtype->id}}">
                                <input type="text" name="relation_name" value="{{$relationtype->relation_name}}" class="form-control" readonly>
{{--                                <select name="name" class="form-control">--}}
{{--                                    <option selected disabled>--- Select Relation ---</option>--}}
{{--                                    @foreach( $relationtype as $ty )--}}
{{--                                        <option value="{{$ty->id}}" {{ ($relation) ?  ($ty->id) == $relation->relation_type_id ? "selected" : "" : (old('name')) == ($ty->id) ? "selected" : "" }}>{{$ty->relation_name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="form-group prw-img">
                                <label class="form-control-label text-uppercase">Image</label>
                                <div class="col-md-3">
                                    <img src="{{($relation) ? url('storage/uploads/relation/') . '/'. $relation->image : ''}}" id="" width="50px">
                                </div>

                                <input type="file" placeholder="Relation Image" name="image" class="form-control uploadFile" value="">
                                @if ($errors->has('image'))
                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


