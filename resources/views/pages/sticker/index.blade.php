@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-10">
                <h2><a href="{{route('category.index')}}"><i class="fa fa-arrow-left" aria-hidden="true">&nbsp&nbsp</i></a>{{$name}}</h2>
            </div>
            <div class="col-md-2">
                <a class="btn btn-lg form-control add-btn pull-right" href="{{ route('sticker.create').'?category_id='.$category_id }}">Create sticker</a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="sticker-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Sticker</th>
                        <th>Status</th>
                        <th>Is_pro</th>
                        <th>Is_color</th>
                        <th>Color code</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
                this.sticker_delete();
                this.change_status();
                this.change_profree();
                this.change_color();
            },
            datatableInit: function(){
                var ajax_url = '';
                ajax_url = "{{ route('sticker.index')}}?api=1&category_id={{$category_id}}";
                var i=1;
                $sticker_table = $('#sticker-table').DataTable({
                    "serverSide": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "id"},
                        {"data": "belongs_to_category.category_id"},
                        {"data": "sticker"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return i++;
                            },
                            "targets": 0,
                        },
                        {
                            "render": function (data, type, row) {
                                let url = "{{ url('storage/uploads/sticker').'/'}}"+row.file_sticker;
                                return  (row.file_sticker) ? `<img height="50" width="50" src="`+url+`" >` : '';
                            },
                            "targets": 1,
                        },
                        {
                            "render": function (data, type, row) {
                                var check = (row.is_enable == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap status chng-status" name="status" id="status" value="1"` + check + ` data-value="` + row.is_enable + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;

                            },
                            "targets": 2,
                        },

                        {
                            "render": function (data, type, row) {
                                var check = (row.is_pro == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap pro-free" name="status" id="is_pro" value="1"` + check + ` data-value="` + row.is_pro + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;

                            },
                            "targets": 3,
                        },
                        {
                            "render": function (data, type, row) {
                                var check = (row.is_color == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap color_status" name="color_status" id="is_color" value="1"` + check + ` data-value="` + row.is_color + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;

                            },
                            "targets": 4,
                        },
                        {
                            "render": function (data, type, row) {
                                return ( row.color_code ) ? row.color_code : '';
                            },
                            "targets": 5,
                        },
                        {
                            "render": function (data, type, row) {
                                let r1 = "{{route('sticker.edit',"__")}}"
                                let r3 = "{{route('sticker.destroy',"__")}}";
                                r1 = r1.replace("__", row.id);
                                r3 = r3.replace("__", row.id);
                                return  `<a href="`+r1+`" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger del-sticker" data-id="`+row.id+`"  type="submit"><i class="fa fa-trash-o"></i></button>`
                            },
                            "targets": 6,
                        },
                    ],
                    "pageLength": 500,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: true,
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function() {
                        i = 0;
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });
                $sticker_table.on( 'row-reorder', function ( e, details, edit ) {

                    var data = [];
                    for ( var i=0; i < details.length ; i++ ) {
                        data[details[i].newPosition] = details[i].node.id;
                    }
                    data = {
                        'items' : data,
                        "_token": "{{ csrf_token() }}",
                    };
                    $.ajax({
                        type: 'POST',
                        url:  "{{ route('sticker.reorder')  }}",
                        data: data,
                        success: function (data) {
                            // $sticker_table.ajax.reload();
                        }
                    })
                });
            },
            sticker_delete: function () {
                $(document).on('click', '.del-sticker', function () {
                    var id = $(this).data('id');
                    swal({
                            title: "Are you sure?",
                            text: "To delete this Sticker...",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            showCancelButton: true,
                            cancelButtonText: 'Cancel',
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            let url = "{{route('sticker.destroy',"__")}}";
                            url = url.replace("__", id);
                            $.ajax({
                                url: url,
                                type: "post",
                                data: {'_method': 'delete', '_token': "{{ csrf_token() }}"},
                                success: function (data) {
                                    swal({
                                        icon: "success",
                                    });
                                    if (data['status'] == "1") {
                                        window.location.reload();
                                    }
                                },
                            });
                        });
                });
            },
            change_status: function () {
                $(document).on('change', '.chng-status', function () {
                    var value = $(this).data('value');
                    var id = $(this).data('id');
                    let url = "{{url('sticker/change_status',"__")}}";
                    url = url.replace("__", id);
                    $.ajax({
                        url: url,
                        type: "post",
                        data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'data': value},
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            },

            change_profree: function () {
                $(document).on('change', '.pro-free', function () {
                    let data = {
                        is_pro: $(this).data('value'),
                        id: $(this).data('id'),
                        "_token": "{{ csrf_token() }}",
                    };

                    $.ajax({
                        url: "{{route('sticker.set_is_pro')}}",
                        type: "post",
                        data: data,
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            },

            change_color: function () {
                $(document).on('change', '.color_status', function () {
                    let data = {
                        is_color: $(this).data('value'),
                        id: $(this).data('id'),
                        "_token": "{{ csrf_token() }}",
                    };

                    $.ajax({
                        url: "{{route('sticker.set_is_color')}}",
                        type: "post",
                        data: data,
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            },
        };

        $(document).ready(function(){
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
