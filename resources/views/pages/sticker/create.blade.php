@extends('layouts.layout')
@section('content')
    <div class="container py-5" style="padding:10px;">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Sticker</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        <form method="post" action="{{($sticker) ? route('sticker.update',$sticker->id) : route('sticker.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if($sticker)
                                @method('patch')
                            @endif
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Sticker Category</label>
                                <input type="hidden" name="category" value="{{$category->id}}">
                                <input type="text" name="category_name" value="{{$category->name}}" class="form-control" readonly>
                                @if ($errors->has('category'))
                                    <span class="text-danger">{{ $errors->first('category') }}</span>
                                @endif
                            </div>

                            <div class="form-group prw-img">
                                <label class="form-control-label text-uppercase">Image</label>
                                <div id="" class="prw-images">
                                    <img src="{{($sticker) ? url('storage/uploads/sticker/') . '/'. $sticker->file_sticker : ''}}"  width="50px">
                                </div>

                                <input type="file" placeholder="Sticker Image" name="{{ (@$sticker) ? 'sticker' : 'sticker[]'}}" class="form-control {{($sticker) ? "uploadFile" : "uploadMultipleFile"}}" value="" {{($sticker) ? "" : "multiple" }} max>

                                @if ($errors->has('sticker'))
                                    <span class="text-danger">{{ $errors->first('sticker') }}</span>
                                @endif
                                @if ($errors->has('sticker.*'))
                                    <span class="text-danger">{{ $errors->first('sticker.*') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Color Code</label>
                                <input type="text" name="color_code" value="{{($sticker) ? $sticker->color_code : old('color_code')}}" class="form-control" placeholder="#ffffff">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
