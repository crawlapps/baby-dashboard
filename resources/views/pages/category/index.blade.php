@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-9">
                <h2>Sticker Category</h2>
            </div>
            <div class="col-md-3">
                <a class="btn btn-lg form-control add-btn pull-right" href="{{ route('category.create') }}">Create Sticker Category</a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="category-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Sticker</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
                this.category_delete();
                this.change_category_lock();
            },
            datatableInit: function(){
                var ajax_url = '';
                ajax_url = "{{ route('category.index')}}?api=1";
                var i=1;
                $stickercategory_table = $('#category-table').DataTable({
                    "serverSide": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "id"},
                        {"data": "name"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                               return i++;
                            },
                            "targets": 0,
                        },
                        {
                            "render": function (data, type, row) {
                                let r1 = "{{route('sticker.index',"__")}}"
                                r1 = r1.replace("__", "category_id="+row.id);
                                return `<a href="`+r1+`" >`+ row.name + `</a>`;
                            },
                            "targets": 1,
                        },
                        {
                            "render": function (data, type, row) {
                                let url = "{{ url('storage/uploads/category').'/'}}" + row.file_category;
                                return (row.file_category) ? `<img src=`+ url +` height="50px" width="50px">` : '';
                            },
                            "targets": 2,
                        },
                        {
                            "render": function (data, type, row) {
                                var check = (row.is_lock == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap status chng-category-lock" name="status" id="status" value="1"` + check + ` data-value="` + row.is_lock + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;
                            },
                            "targets": 3,
                        },
                        {
                            "render": function (data, type, row) {
                                let r1 = "{{route('category.edit',"__")}}"
                                let r3 = "{{route('category.destroy',"__")}}";
                                r1 = r1.replace("__", row.id);
                                r3 = r3.replace("__", row.id);
                                return  `<a href="`+r1+`" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger del-category" data-id="`+row.id+`"  type="submit"><i class="fa fa-trash-o"></i></button>`
                            },
                            "targets": 4,
                        },
                    ],
                    "pageLength": 50,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: true,
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function() {
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });
                $stickercategory_table.on( 'row-reorder', function ( e, details, edit ) {

                    var data = [];
                    for ( var i=0; i < details.length ; i++ ) {
                        data[details[i].newPosition] = details[i].node.id;
                    }
                    data = {
                        'items' : data,
                        "_token": "{{ csrf_token() }}",
                    };
                    $.ajax({
                        type: 'POST',
                        url:  "{{ route('category.reorder')  }}",
                        data: data,
                        success: function (data) {
                            $stickercategory_table.ajax.reload();
                        }
                    })
                });
            },
            category_delete: function () {
                $(document).on('click', '.del-category', function () {
                    var id = $(this).data('id');
                    swal({
                            title: "Are you sure?",
                            text: "To delete this Category...",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            showCancelButton: true,
                            cancelButtonText: 'Cancel',
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            let url = "{{route('category.destroy',"__")}}";
                            url = url.replace("__", id);
                            $.ajax({
                                url: url,
                                type: "post",
                                data: {'_method': 'delete', '_token': "{{ csrf_token() }}"},
                                success: function (data) {
                                    swal({
                                        icon: "success",
                                    });
                                    if (data['status'] == "1") {
                                        window.location.reload();
                                    }
                                },
                            });
                        });
                });
            },
            change_category_lock: function () {
                $(document).on('change', '.chng-category-lock', function () {
                    var value = $(this).data('value');
                    var id = $(this).data('id');
                    let url = "{{url('category/change_lock',"__")}}";
                    url = url.replace("__", id);
                    $.ajax({
                        url: url,
                        type: "post",
                        data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'data': value},
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            },
        };

        $(document).ready(function(){
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
