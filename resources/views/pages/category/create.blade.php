@extends('layouts.layout')
@section('content')
    <div class="container py-5" style="padding:10px;">

        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Category</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        <form method="post" action="{{($category) ? route('category.update',$category->id) : route('category.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if($category)
                                @method('patch')
                            @endif
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Name</label>
                                <input type="text" placeholder="Category name" name="name" class="form-control" value="{{($category) ? $category->name : old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="form-group homescreen prw-img">
                                <label class="form-control-label text-uppercase">Image</label>
                            <div class="col-md-3">
                                <img
                                    src="{{($category) ? url('storage/uploads/category/') . '/'. $category->file_category : ''}}"
                                    id="" width="50px">
                            </div>

                                <input type="file" placeholder="" name="file_category" id="file_category" class="form-control uploadFile" value="{{($category) ? $category->file_category : old('file_category')}}">
                                @if ($errors->has('file_category'))
                                    <span class="text-danger">{{ $errors->first('file_category') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
