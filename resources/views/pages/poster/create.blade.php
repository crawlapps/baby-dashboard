@extends('layouts.layout')
@section('content')
    @php
        $month = [1=>'1', 2=>'February', 3=>'March', 4=>'April', 5=>'May', 6=>"Jun", 7=>"July", 8=>"August", 9=>"September", 10=>"October", 11=>"November", 12=>"December"];
    @endphp
    <div class="container py-5" style="padding:10px;">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Poster</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        <form method="post" action="{{($poster) ? route('poster.update',$poster->id) : route('poster.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if($poster)
                                @method('patch')
                            @endif
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Month</label>

                                <input type="hidden" name="month" value="{{(@$month_int) ? $month_int : ""}}">

                                    @if( @$month_int )
                                    @php  $m = $month_int.' Month Completed'; @endphp
                                        @else
                                    @php   $m = ''; @endphp
                                    @endif

                                <input type="text" name="month_milestone" value="{{$m}}" readonly class="form-control">
{{--                                <select name="month" class="form-control">--}}
{{--                                    <option value="" disabled selected> --- Select Month --- </option>--}}
{{--                                    @for( $i=1; $i<=12; $i++ )--}}
{{--                                        @if($poster)--}}
{{--                                              @php $s =   ($poster->month == $i) ? "selected" : ''; @endphp--}}
{{--                                        @else--}}
{{--                                              @php $s = ( old('month') == $i ) ? "selected" : ''; @endphp--}}
{{--                                        @endif--}}
{{--                                        <option value="{{$i}}" {{$s}}>{{$i}} Month Completed</option>--}}
{{--                                    @endfor--}}
{{--                                    @foreach( $month as $mk=>$mv )--}}
{{--                                        @if($poster)--}}
{{--                                              @php $s =   ($poster->month == $mk) ? "selected" : ''; @endphp--}}
{{--                                        @else--}}
{{--                                              @php $s = ( old('month') == $mk ) ? "selected" : ''; @endphp--}}
{{--                                        @endif--}}
{{--                                            <option value="{{$mk}}" {{$s}}>{{$mv}}</option>--}}
{{--                                        <option value="{{$mk}}" {{ ($poster) ? $poster->month == $mk ? "selected" : (old('month') == $mk) ? "selected" : "" : ""}}>{{$mv}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
                                @if ($errors->has('month'))
                                    <span class="text-danger">{{ $errors->first('month') }}</span>
                                @endif
                            </div>

{{--                            image without baby--}}

                            <div class="form-group prw-img">
                                <label class="form-control-label text-uppercase">Poster ( Without baby )</label>

                                <div id="" class="prw-images">
                                    <img src="{{($poster) ? url('storage/uploads/poster/') . '/'. $poster->poster : ''}}"  width="50px">
                                </div>

                                <input type="file" placeholder="poster Image" name="poster" class="form-control uploadFile" value="" {{($poster) ? "" : "multiple"}}>
                                @if ($errors->has('poster'))
                                    <span class="text-danger">{{ $errors->first('poster') }}</span>
                                @endif
                                @if ($errors->has('poster.*'))
                                    <span class="text-danger">{{ $errors->first('poster.*') }}</span>
                                @endif
                            </div>

{{--                             image with baby--}}

                            <div class="form-group prw-img">
                                <label class="form-control-label text-uppercase">Poster ( With baby )</label>

                                <div id="" class="prw-images">
                                    <img src="{{($poster) ? url('storage/uploads/poster/') . '/'. $poster->file_posterwithbaby : ''}}"  width="50px">
                                </div>

                                <input type="file" placeholder="poster Image" name="file_posterwithbaby" class="form-control uploadFile" value="" {{($poster) ? "" : "multiple"}}>
                                @if ($errors->has('file_posterwithbaby'))
                                    <span class="text-danger">{{ $errors->first('file_posterwithbaby') }}</span>
                                @endif
                                @if ($errors->has('file_posterwithbaby.*'))
                                    <span class="text-danger">{{ $errors->first('file_posterwithbaby.*') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
