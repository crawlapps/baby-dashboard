@extends('layouts.layout')
@section('content')
    <div class="container py-5" style="padding:10px;">

        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Home Screen</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif

                        @if( $homescreen )
                            @php $route = route('homescreen.update',$homescreen->id);
                            @endphp
                        @else
                            @php
                                $route = route('homescreen.store');
                            @endphp
                        @endif
                        @php
                            @endphp
                        <form method="post" id="form_homescreen" enctype="multipart/form-data">
                            @csrf
                            @if($homescreen)
                                @method('patch')
                            @endif
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Type</label>
                                <input type="hidden" name="type" value="{{ (@$type) ? $type : ''}}">
                                @if( $type == "1" )
                                    @php $t = "Single"; @endphp
                                @elseif( $type == "2" )
                                    @php $t = "Relation"; @endphp
                                @elseif( $type == "3" )
                                    @php $t = "collages"; @endphp
                                @endif
                                <input type="text" name="type_name" value="{{$t}}" readonly class="form-control">
                                @if ($errors->has('type'))
                                    <span class="text-danger">{{ $errors->first('type') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Title</label>
                                <input type="text" name="title" value="{{ ( @$homescreen ) ? $homescreen->title : old('title')  }}" class="form-control">
                                <span class="text-danger title"></span>
                            </div>

                            @if( $type == "2" )
                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Relation</label>
                                    <input type="hidden" name="relation_type_id" value="{{($relationtype) ? $relationtype->id : ''}}">
                                    <input type="text" name="relation_name" value="{{($relationtype) ? $relationtype->relation_name : ''}}" readonly class="form-control">
                                        <span class="text-danger relationname">{{ $errors->first('relationname') }}</span>
                                </div>


                                <div class="form-group homescreen prw-img">
                                    <label class="form-control-label text-uppercase">Home Screen Image</label>
                                    <div class="col-md-3">
                                        <img
                                            src="{{($homescreen) ? url('storage/uploads/homescreens/') . '/'. $homescreen->file_homescreen : ''}}"
                                            id="" width="50px">
                                    </div>
                                    <input type="file" placeholder="HomeScreen Image" name="file_homescreen"
                                           class="form-control uploadFile" value="" id="file_homescreen">
                                    <span
                                        class="text-danger file_homescreen">{{ $errors->first('file_homescreen') }}</span>
                                </div>

                                <div class="prw-images">
                                    @if( @$homescreen->relation )
                                        @foreach( $homescreen->relation as $hk=>$val)
                                            <img height="150px" width="150px" src="{{url('storage/uploads/relation/') . '/'. $val->image}}">
                                        @endforeach
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label text-uppercase" >Select All Image For This Relaion</label>

                                    <input type="file" placeholder="Relation Image" name="relation[]" class="form-control uploadMultipleFile" value="" multiple }}>
                                        <span class="text-danger relation"></span>
                                        <span class="text-danger"></span>
                                </div>
                            @endif

                            @if( $type == "1" )
                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Category</label>
                                    <input type="hidden" name="single_category_id" value="{{($singlecategory) ? $singlecategory->id : ''}}">
                                    <input type="text" name="relation_name" value="{{($singlecategory) ? $singlecategory->name : ''}}" readonly class="form-control">
                                    <span class="text-danger categoryname">{{ $errors->first('categoryname') }}</span>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">X</label>
                                    <input type="text" name="x" value="{{ ( @$homescreen ) ? $homescreen->x : old('x')  }}" class="form-control">
                                    <span class="text-danger x"></span>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Y</label>
                                    <input type="text" name="y" value="{{ ( @$homescreen ) ? $homescreen->y : old('y')  }}" class="form-control">
                                    <span class="text-danger y"></span>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Width</label>
                                    <input type="text" name="width" value="{{ ( @$homescreen ) ? $homescreen->width : old('width')  }}" class="form-control">
                                    <span class="text-danger width"></span>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Height</label>
                                    <input type="text" name="height" value="{{ ( @$homescreen ) ? $homescreen->height : old('height')  }}" class="form-control">
                                    <span class="text-danger height"></span>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Ratio</label>
                                    <input type="text" name="ratio" value="{{ ( @$homescreen ) ? $homescreen->ratio : old('ratio')  }}" placeholder="Hint: 16:2" class="form-control">
                                    <span class="text-danger ratio"></span>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Rotation</label>
                                    <input type="text" name="rotation" value="{{ ( @$homescreen ) ? $homescreen->rotation : old('rotation')  }}" placeholder="" class="form-control" value="0">
                                    <span class="text-danger rotation"></span>
                                </div>

                                <div class="form-group homescreen prw-img">
                                    <label class="form-control-label text-uppercase">Home Screen Image ( Transparent )</label>
                                    <div class="col-md-3">
                                        <img
                                            src="{{($homescreen) ? url('storage/uploads/homescreens/') . '/'. $homescreen->file_homescreen : ''}}"
                                            id="" width="50px">
                                    </div>
                                    <input type="file" placeholder="HomeScreen Image" name="file_homescreen"
                                           class="form-control uploadFile" value="" id="file_homescreen">
                                    {{--                                    @if ($errors->has('file_homescreen'))--}}
                                    <span
                                        class="text-danger file_homescreen">{{ $errors->first('file_homescreen') }}</span>
                                    {{--                                    @endif--}}
                                </div>

                                <div class="form-group homescreen prw-img">
                                    <label class="form-control-label text-uppercase">Home Screen Image ( Without Transparent ) </label>
                                    <div class="col-md-3">
                                        <img
                                            src="{{($homescreen) ? url('storage/uploads/homescreens/') . '/'. $homescreen->file_withouttransparent : ''}}"
                                            id="" width="50px">
                                    </div>
                                    <input type="file" placeholder="HomeScreen Image" name="file_withouttransparent"
                                           class="form-control uploadFile" value="" id="file_withouttransparent">
                                    {{--                                    @if ($errors->has('file_homescreen'))--}}
                                    <span
                                        class="text-danger file_withouttransparent">{{ $errors->first('file_withouttransparent') }}</span>
                                    {{--                                    @endif--}}
                                </div>



                            @elseif( $type == "3" )
{{--                                <div class="form-group">--}}
{{--                                    <label class="form-control-label text-uppercase">Category</label>--}}
{{--                                    <input type="hidden" name="collage_category_id" id="collage_category_id" value="{{($collagecategory) ? $collagecategory->id : ''}}">--}}
{{--                                    <input type="text" name="name" value="{{($collagecategory) ? $collagecategory->name : ''}}" readonly class="form-control">--}}
{{--                                    <span class="text-danger categoryname">{{ $errors->first('categoryname') }}</span>--}}
{{--                                </div>--}}

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Enter Grid </label>
                                    <input type="text" id="" placeholder="Collages Grid" name="grid"
                                           class="form-control grid"
                                           value="{{($homescreen) ? $homescreen->grid : old('grid')}}">
                                    {{--                                    @if ($errors->has('grid'))--}}
                                    <span class="text-danger grid">{{ $errors->first('grid') }}</span>
                                    {{--                                    @endif--}}
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Select Grid ( with baby )</label>
                                    <input type="file" id="gridFile" placeholder="Collages Grid" name="file_grid"
                                           class="form-control file_grid uploadFile"
                                           value="">

                                    <input type="hidden" name="hdn_file_grid" id="hdn_file_grid"
                                           value="{{($homescreen) ? $homescreen->file_grid : ""}}">
                                    {{--                                    @if ($errors->has('grid'))--}}
                                    <span class="text-danger file_grid"></span>
                                    {{--                                    @endif--}}
                                </div>
                                <div id="grid_preview" class="col-md-6 prw-img" style="display: {{  ($homescreen) ? 'block' : 'none' }}">
                                    <img
                                        src="{{($homescreen) ? url('storage/uploads/grid/') . '/'. $homescreen->file_grid : ''}}"
                                        id="grid_image" width="200px" height="200px">
                                </div>
                            <div class="elements" style="display:{{(@$homescreen) ? 'block' : 'none' }};">
                                @if( @$homescreen )
                                    <label class="form-control-label text-uppercase">Enter elements for grid</label>
                                    @for( $i=0;$i<$homescreen->grid; $i++ )
                                        <div class="row">
                                            <div class="col-md-2"><label class="form-control-label text-uppercase">Position {{ $i }}</label></div>
                                            <div class="col-md-1"><label class="form-control-label text-uppercase">X</label></div>
                                            <div class="col-md-1"><label class="form-control-label text-uppercase">Y</label></div>
                                            <div class="col-md-1"><label class="form-control-label text-uppercase">Width</label></div>
                                            <div class="col-md-1"><label class="form-control-label text-uppercase">Height</label></div>
                                            <div class="col-md-1"><label class="form-control-label text-uppercase">Rotation</label></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2   form-group"></div>
                                            <div class="col-md-1"><input type="text" name="position[{{$i}}][x]" class="form-control x{{$i}}"></div>
                                            <div class="col-md-1"><input type="text" name="position[{{$i}}][y]" class="form-control y{{$i}}"></div>
                                            <div class="col-md-1"><input type="text" name="position[{{$i}}][width]" class="form-control width{{$i}}"></div>
                                            <div class="col-md-1"><input type="text" name="position[{{$i}}][height]" class="form-control height{{$i}}"></div>
                                            <div class="col-md-1"><input type="text" name="position[{{$i}}][rotation]"  value=0 class="form-control rotation{{$i}}"></div>
                                        </div>
                                    @endfor
                                @endif

                            </div>
                                <span class="text-danger elements-err"></span>

                                <div class="form-group">
                                    <label class="form-control-label text-uppercase">Ratio</label>
                                    <input type="text" name="ratio" value="{{ ( @$homescreen ) ? $homescreen->ratio : old('ratio')  }}" placeholder="Hint: 16:2" class="form-control">
                                    <span class="text-danger ratio"></span>
                                </div>

                                <div class="form-group homescreen prw-img">
                                    <label class="form-control-label text-uppercase"> Without Baby Image </label>
                                    <div class="col-md-3">
                                        <img
                                            src="{{($homescreen) ? url('storage/uploads/homescreens/') . '/'. $homescreen->file_withouttransparent : ''}}"
                                            id="" width="50px">
                                    </div>
                                    <input type="file" placeholder="HomeScreen Image" name="file_withoutbaby"
                                           class="form-control uploadFile" value="" id="file_withoutbaby">
                                    {{--                                    @if ($errors->has('file_homescreen'))--}}
                                    <span
                                        class="text-danger file_withoutbaby">{{ $errors->first('file_withoutbaby') }}</span>
                                    {{--                                    @endif--}}
                                </div>


                                {{--                                <div class="positions" style="display:{{(@$homescreen->is_position == 1) ? 'block' : 'none' }};">--}}
{{--                                <div class="positions" style="display:{{($homescreen) ? 'block' : 'none' }};">--}}
{{--                                    --}}{{--                                    @if( @$homescreen->is_position == 1)--}}
{{--                                    @if( @$homescreen)--}}
{{--                                        <div><label class="form-control-label text-uppercase">Collages Positions</label>--}}
{{--                                        </div>--}}

{{--                                        @for( $i=0;$i<$homescreen->grid; $i++ )--}}
{{--                                            <div class="row prw-img"><label--}}
{{--                                                    class="col-md-1 form-control-label text-uppercase">{{$i}}</label>--}}
{{--                                                <input type=checkbox name="collage[{{$i}}][position]" value="{{$i}}"--}}
{{--                                                       class="col-md-1 form-control show_file position{{$i}}"--}}
{{--                                                       style="height:15px;" {{ ( @$homescreen->_collage[$i] ) ? ($i == $homescreen->_collage[$i]->position) ? "checked" : "" : ""}}>--}}

{{--                                                <div class="col-md-2">--}}
{{--                                                    <img src="" id="collage_image{{$i}}" width="50px">--}}
{{--                                                </div>--}}
{{--                                                <input type=file name="collage[{{$i}}][image]"--}}
{{--                                                       class="uploadFile col-md-6 form-control collage_image{{$i}}"--}}
{{--                                                       data-position={{$i}} style="display:none;"></div>--}}
{{--                                        @endfor--}}

{{--                                    @endif--}}
{{--                                </div>--}}
{{--                                <span class="text-danger collages"></span>--}}
                            @endif

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Is_home</label>
                                <label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap" {{ (@$homescreen) ? ($homescreen->is_home == '1') ? "checked" : '' : ''  }} name="is_home" id="is_home" value="1">
                                    <span class="lable"></span></label>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.formsubmit();
                this.addposition();
                // this.imagepreview();
                this.checkcollages();
            },
            addposition: function () {
                // $(document).on('keyup', '.grid', function () {
                //     $('#grid_image').attr('src', '');
                //     $('#hdn_file_grid').val('');
                //
                //     var grid = $(this).val();
                //
                //     var position = '<div><label class="form-control-label text-uppercase">Collages Positions</label>';
                //     for (var i = 0; i < grid; i++) {
                //         position += '<div class="row prw-img"><label class="form-control-label col-md-1 text-uppercase">' + i + '</label>';
                //         position += '<input type=checkbox name="collage[' + i + '][position]" value=' + i + ' class="col-md-1 form-control show_file" style="height:15px;">';
                //         position += '<div class="col-md-2"><img src="" id="collage_image' + i + '" width="50px"></div>'
                //         position += '<input type=file name="collage[' + i + '][image]" class="uploadFile col-md-6 form-control collage_image' + i + '" data-position="' + i + '" style="display:none;">';
                //         position += '</div>';
                //     }
                //     position += '</div>';
                //
                //     $('.positions').html(position);
                // });

                $(document).on('keyup', '.grid', function () {
                    $('#grid_image').attr('src', '');
                    $('#hdn_file_grid').val('');

                    var grid = $(this).val();

                    var element = `<label class="form-control-label text-uppercase">Enter elements for grid</label>`;
                    for (var i = 0; i < grid; i++) {
                        element += `<div class="row">
                                        <div class="col-md-2"><label class="form-control-label text-uppercase">Position ` + i + `</label></div>`;
                        element += `<div class="col-md-1"><label class="form-control-label text-uppercase">X</label></div>`;
                        element += `<div class="col-md-1"><label class="form-control-label text-uppercase">Y</label></div>`;
                        element += `<div class="col-md-1"><label class="form-control-label text-uppercase">Width</label></div>`;
                        element += `<div class="col-md-1"><label class="form-control-label text-uppercase">Height</label></div>`;
                        element += `<div class="col-md-1"><label class="form-control-label text-uppercase">Rotation</label></div></div>`;

                        element += `<div class="row">`;
                        element += `<div class="col-md-2 form-group"></div>`;
                        element += `<div class="col-md-1"><input type="text" name="position[`+ i +`][x]" class="form-control x`+ i +`"></div>`;
                        element += `<div class="col-md-1"><input type="text" name="position[`+ i +`][y]" class="form-control y`+ i +`"></div>`;
                        element += `<div class="col-md-1"><input type="text" name="position[`+ i +`][width]" class="form-control width`+ i +`"></div>`;
                        element += `<div class="col-md-1"><input type="text" name="position[`+ i +`][height]" class="form-control height`+ i +`"></div>`;
                        element += `<div class="col-md-1"><input type="text" name="position[`+ i +`][rotation]" value=0 class="form-control rotation`+ i +`"></div></div>`;
                    }
                    element += `</div>`;

                    $('.elements').html(element);
                });

                $(document).on('click', '.show_file', function () {
                    var val = $(this).val();
                    if ($(this).prop("checked") == true) {
                        $('#collage_image' + val ).css('display', 'block');
                        $('.collage_image' + val).css('display', 'block');
                    } else {
                        $('.collage_image' + val).css('display', 'none');
                        $('#collage_image' + val ).css('display', 'none');

                    }
                });
            },

            checkcollages: function () {
                var collage = `@php echo ( @$homescreen ) ? $homescreen->_collage : "[]" @endphp`

                if( collage != "" ){
                    var collage_array = JSON.parse(collage);
                }

                // return false;
                $.each(collage_array, function (index, value) {
                    $('.x'+index).val(value['x']);
                    $('.y'+index).val(value['y']);
                    $('.width'+index).val(value['width']);
                    $('.height'+index).val(value['height']);
                    {{--$( '.position' + value['position']).attr('checked', true);--}}
                    {{--$('.collage_image' + value['position']).css('display', 'block');--}}
                    {{--var image = value['image'];--}}
                    {{--$('#collage_image' + value['position']).attr('src', "{{url('storage/uploads/collages/')}}" + "/" + image);--}}
                });
            },

            formsubmit: function () {
                $('#form_homescreen').on('submit', function (e) {

                    e.preventDefault();
                    e.stopPropagation();
                    var form_data = new FormData(this);
                    $.ajax({
                        url: "{{$route}}",
                        type: "post",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data) {
                            $('.file_homescreen').html("");
                            $('.file_withouttransparent').html("");
                            $('.collages').html("");
                            $('.grid').html("");
                            $('.file_grid').html("");
                            $('.relation').html("");
                            $('.relationname').html("");
                            $('.title').html("");
                            $('.x').html("");
                            $('.y').html("");
                            $('.height').html("");
                            $('.width').html("");
                            $('.ratio').html("");
                            $('.elements-err').html("");
                            $('.file_withoutbaby').html("");
                            if (typeof data['redirect'] !== 'undefined') {
                                window.location.href = data['redirect'];
                            }
                        },
                        error: function (data) {
                            console.log(data);
                            if (data.status === 422) {
                                // var errors = data.responseJSON;
                                var first_key = '';

                                $('.file_homescreen').html("");
                                $('.file_withouttransparent').html("");
                                $('.collages').html("");
                                $('.grid').html("");
                                $('.file_grid').html("");
                                $('.relation').html("");
                                $('.relationname').html("");
                                $('.title').html("");
                                $('.x').html("");
                                $('.y').html("");
                                $('.height').html("");
                                $('.width').html("");
                                $('.ratio').html("");
                                $('.elements-err').html("");
                                $('.file_withoutbaby').html("");

                                $.each(data.responseJSON, function (key, value) {
                                    if (key == 'file_homescreen') {
                                        $('.file_homescreen').html(value);
                                    } else if (key == 'collages') {
                                        $('.collages').html(value);
                                    } else if (key == 'grid') {
                                        $('.grid').html(value);
                                    } else if (key == 'file_grid' || key == 'hdn_file_grid') {
                                        $('.file_grid').html(value);
                                    }else if (key == 'relation') {
                                        $('.relation').html(value);
                                    }else if (key == 'relation_type_id') {
                                        $('.relationname').html(value);
                                    }else if (key == 'relation.0') {
                                        $('.relation').html(value);
                                    }else if (key == 'title') {
                                        $('.title').html(value);
                                    }else if (key == 'x') {
                                        $('.x').html(value);
                                    }else if (key == 'y') {
                                        $('.y').html(value);
                                    }else if (key == 'height') {
                                        $('.height').html(value);
                                    }else if (key == 'width') {
                                        $('.width').html(value);
                                    }else if( key == 'file_withouttransparent' ){
                                        $('.file_withouttransparent').html(value);
                                    }else if( key == 'ratio' ){
                                        $('.ratio').html(value);
                                    }else if( key == 'file_withoutbaby' ){
                                        $('.file_withoutbaby').html(value);
                                    }else if( key.startsWith("position") ){
                                        if( first_key != key.substring(0, key.lastIndexOf('.')) ){
                                            $('.elements-err').append('<p>' + value + '</p>');
                                        }
                                        first_key = key.substring(0, key.lastIndexOf('.'));
                                    }
                                });
                            }
                        }
                    });
                });
            },

        };
        $(document).ready(function () {
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
