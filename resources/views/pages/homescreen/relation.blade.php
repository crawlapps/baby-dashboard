@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-9">
                <h2><a href="{{route('relation.index')}}?id={{$relation_type_id}}"><i class="fa fa-arrow-left" aria-hidden="true">&nbsp&nbsp</i></a>Relation Images</h2>
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="relationhomescreen-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Image</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
            },
            datatableInit: function(){
                var ajax_url = '';
                ajax_url = "{{ url('homescreen_relation')}}?api=1&home_screen_id={{$home_screen_id}}";
                var i=1;
                $homescreen_table = $('#relationhomescreen-table').DataTable({
                    "serverSide": true,
                    "stateSave": true,
                    "processing": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "id"},
                        {"data": "image"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return  i++;
                            },
                            "targets": 0,
                        },

                        {
                            "render": function (data, type, row) {
                                var image = (row.image) ? row.image : '';
                                let url = "{{ url('storage/uploads/relation/').'/'}}"+image;

                                return (row.image) ? `<img style="background-color: #5d5959" height="50" width="50" src="`+url+`" >` : '';
                            },
                            "targets": 1,
                        },
                    ],
                    "pageLength": 50,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: {
                        "update": false,
                    },
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function() {
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });
                $homescreen_table.on( 'row-reorder', function ( e, details, edit ) {

                    var data = [];
                    for ( var i=0; i < details.length ; i++ ) {
                        data[details[i].newPosition] = details[i].node.id;
                    }
                    data = {
                        'items' : data,
                        "_token": "{{ csrf_token() }}",
                    };
                    $.ajax({
                        type: 'POST',
                        url:  "{{ route('relation_image.reorder')  }}",
                        data: data,
                        success: function (data) {
                            $homescreen_table.ajax.reload();
                        }
                    })
                });

            },
        };

        $(document).ready(function(){
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
