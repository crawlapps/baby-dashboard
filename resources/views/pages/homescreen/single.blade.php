@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-9">
                <h2><a href="{{route('singlecategory.index')}}"><i class="fa fa-arrow-left" aria-hidden="true">&nbsp&nbsp</i></a>{{$category->name}}</h2>
            </div>
            <div class="col-md-3">
                <a class="btn btn-lg form-control add-btn pull-right" href="{{ route('homescreen.create').'?type=1&single_category_id='.$category->id }}">Create homescreen</a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="singlehomescreen-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Transparent</th>
                        <th>Without Transparent</th>
                        <th>X</th>
                        <th>Y</th>
                        <th>Width</th>
                        <th>Height</th>
                        <th>Ratio</th>
                        <th>Rotation</th>
                        <th>Is_Active</th>
                        <th>Is_home</th>
                        <th>Is_pro</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
                this.homescreen_delete();
                this.change_status();
                this.change_profree();
                this.change_activestatus();
            },
            datatableInit: function(){
                var ajax_url = '';
                ajax_url = "{{ url('singlehomescreen')}}?api=1&type=1&id={{Request::get('id')}}";
                var i=1;
                $homescreen_table = $('#singlehomescreen-table').DataTable({
                    "serverSide": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "id"},
                        {"data": "title"},
                        {"data": "x"},
                        {"data": "y"},
                        {"data": "height"},
                        {"data": "width"},
                        {"data": "ratio"},
                        {"data": "rotation"},
                        {"data": "file_homescreen"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return  i++;
                            },
                            "targets": 0,
                        },

                        {
                            "render": function (data, type, row) {
                                var image = (row.file_homescreen) ? row.file_homescreen : '';
                                let url = (row.file_homescreen) ? "{{ url('storage/uploads/homescreens').'/'}}"+image : "{{ env('NO_IMAGE_PATH')  }}";

                                return (row.file_homescreen) ? `<img height="50" width="50" src="`+url+`" >` : '';

                                // return  (row.collage) ?  : '';
                            },
                            "targets": 2,
                        },
                        {
                            "render": function (data, type, row) {
                                var image = (row.file_withouttransparent) ? row.file_withouttransparent : '';
                                let url = (row.file_withouttransparent) ? "{{ url('storage/uploads/homescreens').'/'}}"+image : "{{ env('NO_IMAGE_PATH')  }}";

                                return (row.file_withouttransparent) ? `<img height="50" width="50" src="`+url+`" >` : '';
                            },
                            "targets": 3,
                        },

                        {
                            "render": function (data, type, row) {
                                return  row.x;
                            },
                            "targets": 4,
                        },
                        {
                            "render": function (data, type, row) {
                                return  row.y;
                            },
                            "targets": 5,
                        },

                        {
                            "render": function (data, type, row) {
                                return  row.width;
                            },
                            "targets": 6,
                        },
                        {
                            "render": function (data, type, row) {
                                return  row.height;
                            },
                            "targets": 7,
                        },
                        {
                            "render": function (data, type, row) {
                                return  row.ratio;
                            },
                            "targets": 8,
                        },
                        {
                            "render": function (data, type, row) {
                                return  row.rotation;
                            },
                            "targets": 9,
                        },
                        {
                            "render": function (data, type, row) {
                                var check = (row.is_active == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap status chng-activestatus" name="activestatus" id="activestatus" value="1"` + check + ` data-value="` + row.is_active + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;

                            },
                            "targets": 10,
                        },
                        {
                            "render": function (data, type, row) {
                                var check = (row.is_home == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap status chng-status" name="status" id="status" value="1"` + check + ` data-value="` + row.is_home + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;

                            },
                            "targets": 11,
                        },
                        {
                            "render": function (data, type, row) {
                                var check = (row.is_pro == 1) ? 'checked' : '';
                                return `<label class="label-switch switch-success">
                                    <input type="checkbox" class="switch switch-bootstrap pro-free" name="status" id="is_pro" value="1"` + check + ` data-value="` + row.is_pro + `" data-id="`+ row.id +`">
                                    <span class="lable"></span></label>`;

                            },
                            "targets": 12,
                        },

                        {
                            "render": function (data, type, row) {
                                let r1 = "{{route('homescreen.edit',"__")}}"
                                let r3 = "{{route('homescreen.destroy',"__")}}";
                                r1 = r1.replace("__", row.id);
                                r3 = r3.replace("__", row.id);
                                return  `<a href="`+r1+`" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                <button class="btn btn-danger del-homescreen" data-id="`+row.id+`" type="submit"><i class="fa fa-trash-o"></i></button>`
                            },
                            "targets": 13,
                        },
                    ],
                    "pageLength": 50,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: true,
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function() {
                        i=0;
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });
                $homescreen_table.on( 'row-reorder', function ( e, details, edit ) {

                    var data = [];
                    for ( var i=0; i < details.length ; i++ ) {
                        data[details[i].newPosition] = details[i].node.id;
                    }
                    data = {
                        'items' : data,
                        "_token": "{{ csrf_token() }}",
                    };
                    $.ajax({
                        type: 'POST',
                        url:  "{{ route('homescreen.reorder')  }}",
                        data: data,
                        success: function (data) {
                            // $homescreen_table.ajax.reload();
                        }
                    })
                });

            },
            homescreen_delete: function () {
                $(document).on('click', '.del-homescreen', function () {
                    var id = $(this).data('id');
                    swal({
                            title: "Are you sure?",
                            text: "To delete this homescreen...",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            showCancelButton: true,
                            cancelButtonText: 'Cancel',
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            let url = "{{route('homescreen.destroy',"__")}}";
                            url = url.replace("__", id);
                            $.ajax({
                                url: url,
                                type: "post",
                                data: {'_method': 'delete', '_token': "{{ csrf_token() }}"},
                                success: function (data) {
                                    swal({
                                        icon: "success",
                                    });
                                    if (data['status'] == "1") {
                                        window.location.reload();
                                    }
                                },
                            });
                        });
                });
            },

            change_status: function () {
                $(document).on('change', '.chng-status', function () {
                    var value = $(this).data('value');
                    var id = $(this).data('id');
                    let url = "{{url('homescreen/change_status',"__")}}";
                    url = url.replace("__", id);
                    $.ajax({
                        url: url,
                        type: "post",
                        data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'data': value},
                        success: function (data) {
                            console.log(data);
                            crawlapps_baby_dashboard.datatableInit();
                            // window.location.reload();
                        },
                    });
                });
            },

            change_activestatus: function () {
                $(document).on('change', '.chng-activestatus', function () {
                    var value = $(this).data('value');
                    var id = $(this).data('id');
                    let url = "{{url('homescreen/change_activestatus',"__")}}";
                    url = url.replace("__", id);
                    $.ajax({
                        url: url,
                        type: "post",
                        data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'data': value},
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            },

            change_profree: function () {
                $(document).on('change', '.pro-free', function () {
                    let data = {
                        is_pro: $(this).data('value'),
                        id: $(this).data('id'),
                        "_token": "{{ csrf_token() }}",
                    };

                    $.ajax({
                        url: "{{route('homescreen.set_is_pro')}}",
                        type: "post",
                        data: data,
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            },
        };

        $(document).ready(function(){
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
