@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-9">
                <h2>Homescreen</h2>
            </div>
            <div class="col-md-3">
                <a class="btn btn-lg form-control add-btn pull-right" href="{{ route('homescreen.create') }}">Create homescreen</a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="homescreen-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Type</th>
                        <th>Position</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
                this.homescreen_delete();
            },
            datatableInit: function(){
                var ajax_url = '';
                ajax_url = "{{ route('homescreen.index')}}?api=1";
                var i=1;
                $homescreen_table = $('#homescreen-table').DataTable({
                    "serverSide": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "id"},
                        {"data": "type"},
                        {"data": "collage.position"},
                        {"data": "collage.image"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return  i++;
                            },
                            "targets": 0,
                        },
                        {
                            "render": function (data, type, row) {
                                var type = (row.type == "1") ? "Single Image" : (row.type == "2") ? "Relation" : "Collages";
                                return  (row.type) ? type : '';
                            },
                            "targets": 1,
                        },
                        {
                            "render": function (data, type, row) {
                                return  (row.collage) ? row.collage.position : '';
                            },
                            "targets": 2,
                        },
                        {
                            "render": function (data, type, row) {
                                var image = (row.collage) ? row.collage.image : '';
                                let url = "{{ url('storage/uploads/collages/').'/'}}"+image;

                                return (row.collage) ? `<img height="50" width="50" src="`+url+`" >` : '';

                                // return  (row.collage) ?  : '';
                            },
                            "targets": 3,
                        },
                        {
                            "render": function (data, type, row) {
                                let r1 = "{{route('homescreen.edit',"__")}}"
                                let r3 = "{{route('homescreen.destroy',"__")}}";
                                r1 = r1.replace("__", row.id);
                                r3 = r3.replace("__", row.id);
                                return  `<a href="`+r1+`" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                <button class="btn btn-danger del-homescreen" data-id="`+row.id+`" type="submit"><i class="fa fa-trash-o"></i></button>`
                            },
                            "targets": 4,
                        },
                    ],
                    "pageLength": 100,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: true,
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function() {
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });

            },
            homescreen_delete: function () {
                $(document).on('click', '.del-homescreen', function () {
                    var id = $(this).data('id');
                    swal({
                            title: "Are you sure?",
                            text: "To delete this homescreen...",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            showCancelButton: true,
                            cancelButtonText: 'Cancel',
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            let url = "{{route('homescreen.destroy',"__")}}";
                            url = url.replace("__", id);
                            $.ajax({
                                url: url,
                                type: "post",
                                data: {'_method': 'delete', '_token': "{{ csrf_token() }}"},
                                success: function (data) {
                                    swal({
                                        icon: "success",
                                    });
                                    if (data['status'] == "1") {
                                        window.location.reload();
                                    }
                                },
                            });
                        });
                });
            },
        };

        $(document).ready(function(){
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
