
@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-9">
                <h2><a href="{{route('collagehomescreen')}}?id={{$collage_category}}"><i class="fa fa-arrow-left" aria-hidden="true">&nbsp&nbsp</i></a>Collage Position elements</h2>
            </div>

            <div class="col-md-12">
                <table class="table table-striped" id="collageselement-table">
                    <thead>
                    <tr>
                        <th>Position</th>
                        <th>X</th>
                        <th>Y</th>
                        <th>Width</th>
                        <th>Height</th>
                        <th>Rotation</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
            },
            datatableInit: function () {
                var ajax_url = '';
                ajax_url = "{{ route('collageelements')}}?api=1&type=3&id={{Request::get('id')}}";
                var i = 1;
                $homescreen_table = $('#collageselement-table').DataTable({
                    "serverSide": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "position"},
                        {"data": "x"},
                        {"data": "y"},
                        {"data": "width"},
                        {"data": "height"},
                        {"data": "rotation"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return  row.position;
                            },
                            "targets": 0,
                        },
                    ],
                    "pageLength": 10,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: true,
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function () {
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });
                $homescreen_table.on( 'row-reorder', function ( e, details, edit ) {
                    var data = [];
                    for ( var i=0; i < details.length ; i++ ) {
                        data[details[i].newPosition] = details[i].node.id;
                    }
                    data = {
                        'items' : data,
                        "_token": "{{ csrf_token() }}",
                    };
                    $.ajax({
                        type: 'POST',
                        url:  "{{ route('collageelements.reorder')  }}",
                        data: data,
                        success: function (data) {
                            $homescreen_table.ajax.reload();
                        }
                    })
                });
            },
        }

        $(document).ready(function(){
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
