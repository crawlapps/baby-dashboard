@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-9">
                <h2>Homescreens</h2>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="homescreen-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Type</th>
                        <th>Title</th>
                        <th>Image</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var crawlapps_baby_dashboard = {
            init: function () {
                this.datatableInit();
            },
            datatableInit: function(){
                var ajax_url = '';
                ajax_url = "{{ route('homescreen.index')}}?api=1";
                var i=1;
                $homescreen_table = $('#homescreen-table').DataTable({
                    "serverSide": true,
                    "destroy": true,
                    "ajax": ajax_url,
                    "columns": [
                        {"data": "id"},
                        {"data": "title"},
                        {"data": "file_homescreen"},
                    ],
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return  i++;
                            },
                            "targets": 0,
                        },
                        {
                            "render": function (data, type, row) {
                                var type = ( row.type == "1") ? "Single" : (row.type == "2") ? "Relation" : "Collage";
                                return type;
                            },
                            "targets": 1,
                        },
                        {
                            "render": function (data, type, row) {
                                return row.title;
                            },
                            "targets": 2,
                        },

                        {
                            "render": function (data, type, row) {
                                var image = (row.file_homescreen) ? row.file_homescreen : (row.file_grid) ? row.file_grid : '';
                                let url = (row.file_homescreen) ? "{{ url('storage/uploads/homescreens/').'/'}}"+image : (row.file_grid) ? "{{ url('storage/uploads/grid/').'/'}}"+image : '';

                                return (row.file_homescreen || row.file_grid) ? `<img height="50" width="50" src="`+url+`" >` : '';
                            },
                            "targets": 3,
                        },
                    ],
                    "pageLength": 100,
                    "lengthChange": false,
                    "orderable": false,
                    "bSort": false,
                    "bInfo": false,

                    rowReorder: false,
                    'createdRow': function (row, data, dataIndex) {
                        $(row).attr({
                            'id': data.id,
                            /*'data-webapp_code': data.webapp_id,*/
                        });
                    },
                    "fnDrawCallback": function() {
                        // $('.input-checkbox').bootstrapToggle();
                    },
                });

            },
        };

        $(document).ready(function(){
            crawlapps_baby_dashboard.init();
        });
    </script>
@endsection
