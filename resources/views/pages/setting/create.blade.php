@extends('layouts.layout')
@section('content')
    <div class="container py-5" style="padding:10px;">

        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Setting</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        <form method="post" action="{{ route('setting.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Key</label>
                                <input type="text" placeholder="Enter Key" name="key" class="form-control" value="{{ old('key')  }}">
                                @if ($errors->has('key'))
                                    <span class="text-danger">{{ $errors->first('key') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
