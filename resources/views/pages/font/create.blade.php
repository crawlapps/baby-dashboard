@extends('layouts.layout')
@section('content')
    <div class="container py-5" style="padding:10px;">

        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Font</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        <form method="post" action="{{($font) ? route('font.update',$font->id) : route('font.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if($font)
                                @method('patch')
                            @endif
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Name</label>
                                <input type="text" placeholder="Font name" name="name" class="form-control" value="{{($font) ? $font->name : old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="form-group homescreen prw-img">
                                <label class="form-control-label text-uppercase">File</label>
                                <div class="col-md-3">
                                   {{($font) ? $font->file_font : ''}}
                                </div>

                                <input type="file" placeholder="" name="file_font" id="file_font" class="form-control uploadFile" value="{{($font) ? $font->file_font : old('file_font')}}">
                                @if ($errors->has('file_font'))
                                    <span class="text-danger">{{ $errors->first('file_font') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
