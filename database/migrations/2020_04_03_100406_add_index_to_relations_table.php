<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relations', function (Blueprint $table) {
            $table->index('relation_type_id','idx_relation_relationtype');
            $table->index('home_screen_id','idx_relation_home');
            $table->index('id','idx_relation');
        });
        Schema::table('home_screens', function (Blueprint $table) {
            $table->renameIndex('idx_relation_home', 'idx_home_relationtype');
            $table->renameIndex('idx_collage_home', 'idx_collage_home');
        });
        Schema::table('backgrounds', function (Blueprint $table) {
            $table->index('background_category_id','idx_background_backgroundCategory');
            $table->index('id','idx_background');
        });
        Schema::table('monthly_collages', function (Blueprint $table) {
            $table->index('home_screen_id','idx_monthlyCollages_home');
            $table->index('id','idx_monthlyCollages');
        });
        Schema::table('stickers', function (Blueprint $table) {
            $table->index('category_id','idx_sticker_category');
            $table->index('id','idx_sticker');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relations', function (Blueprint $table) {
            //
        });
    }
}
