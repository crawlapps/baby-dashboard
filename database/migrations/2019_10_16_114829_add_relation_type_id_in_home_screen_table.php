<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationTypeIdInHomeScreenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            $table->unsignedBigInteger('relation_type_id')->after('id')->nullable();
        });
        Schema::table('home_screens', function (Blueprint $table) {
            $table->foreign('relation_type_id')->references('id')->on('relation_type')->onDelete('SET NULL')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            $table->removeColumn('relation_type_id');
        });
    }
}
