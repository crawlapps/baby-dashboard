<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackgroundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backgrounds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('background_category_id')->nullable();
            $table->string('file_background')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('backgrounds', function (Blueprint $table) {
            $table->foreign('background_category_id')->references('id')->on('background_categories')->onDelete('SET NULL')->onUpdate('NO ACTION');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backgrounds');
    }
}
