<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFileWithouttransperentToHomeScreensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            $table->string('file_withouttransparent')->after('file_homescreen')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            $table->removeColumn('file_withouttransparent');
        });
    }
}
