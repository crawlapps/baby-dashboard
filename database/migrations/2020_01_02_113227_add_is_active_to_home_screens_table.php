<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsActiveToHomeScreensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            $table->boolean('is_active')->after('is_pro')->default(0)->nullable();
            $table->removeColumn('batch');
        });
        Schema::table('backgrounds', function (Blueprint $table) {
            $table->boolean('is_active')->after('file_background')->default(0)->nullable();
        });
        Schema::table('background_categories', function (Blueprint $table) {
            $table->boolean('is_active')->after('file_background_category')->default(0)->nullable();
        });
        Schema::table('single_categories', function (Blueprint $table) {
            $table->boolean('is_active')->after('name')->default(0)->nullable();
        });
        Schema::table('relation_type', function (Blueprint $table) {
            $table->boolean('is_active')->after('relation_name')->default(0)->nullable();
        });
        Schema::table('collage_categories', function (Blueprint $table) {
            $table->boolean('is_active')->after('name')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            //
        });
    }
}
