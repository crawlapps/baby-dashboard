<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyInRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relations', function (Blueprint $table) {
            $table->unsignedBigInteger('home_screen_id')->after('relation_type_id')->nullable();
        });

        Schema::table('relations', function (Blueprint $table) {
            $table->foreign('home_screen_id')->references('id')->on('home_screens')->onDelete('SET NULL')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relations', function (Blueprint $table) {
            $table->removeColumn('home_screen_id');
        });
    }
}
