<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monthly_posters', function (Blueprint $table) {
            $table->renameColumn('month','month_id');
        });

        Schema::table('monthly_posters', function (Blueprint $table) {
            $table->unsignedBigInteger('month_id')->change();
        });

        Schema::create('month', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('month');
            $table->timestamps();
        });
        Schema::table('monthly_posters', function (Blueprint $table) {
           $table->foreign('month_id')->references('id')->on('month')->onDelete('SET NULL')->onUpdate('NO ACTION');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('month');
    }
}
