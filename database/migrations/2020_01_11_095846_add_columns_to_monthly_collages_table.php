<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToMonthlyCollagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monthly_collages', function (Blueprint $table) {
            $table->float('x')->default(0)->after('position');
            $table->float('y')->default(0)->after('x');
            $table->float('width')->default(0)->after('y');
            $table->float('height')->default(0)->after('width');
        });

        Schema::table('monthly_posters', function (Blueprint $table) {
            $table->string('file_posterwithbaby')->nullable()->after('poster');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monthly_collages', function (Blueprint $table) {
            //
        });
    }
}
