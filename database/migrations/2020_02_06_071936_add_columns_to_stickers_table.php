<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToStickersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stickers', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_pro');
        });
        Schema::table('home_screens', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_active');
        });
        Schema::table('monthly_posters', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_pro');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_pro');
        });
        Schema::table('background_categories', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_active');
        });
        Schema::table('backgrounds', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_pro');
        });
        Schema::table('relation_type', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_active');
        });
        Schema::table('single_categories', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_active');
        });
        Schema::table('collage_categories', function (Blueprint $table) {
            $table->integer('order_by')->default(0)->nullable()->after('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stickers', function (Blueprint $table) {
            //
        });
    }
}
