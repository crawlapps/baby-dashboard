<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSingleCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('single_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('home_screens', function (Blueprint $table) {
            $table->unsignedBigInteger('single_category_id')->after('relation_type_id')->nullable();
            $table->double('x')->after('is_position')->default(0);
            $table->double('y')->after('x')->default(0);
            $table->double('height')->after('y')->default(0);
            $table->double('width')->after('height')->default(0);
            $table->boolean('is_home')->after('width')->default(0)->comment('0 = false, 1 = true');
        });

        Schema::table('home_screens', function (Blueprint $table) {
            $table->foreign('single_category_id')->references('id')->on('single_categories')->onDelete('SET NULL')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('single_categories');
    }
}
