<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsProToHomescreenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            $table->boolean('is_pro')->after('is_home')->default(0);
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->boolean('is_pro')->after('is_lock')->default(0);
        });
        Schema::table('monthly_posters', function (Blueprint $table) {
            $table->boolean('is_pro')->after('is_enable')->default(0);
        });
        Schema::table('stickers', function (Blueprint $table) {
            $table->boolean('is_pro')->after('is_enable')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('homescreen', function (Blueprint $table) {
            //
        });
    }
}
