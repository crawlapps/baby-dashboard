<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGridColumnToHomeScreensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            $table->integer('grid')->after('file_homescreen')->nullable();
            $table->text('file_grid')->after('grid')->nullable();
            $table->integer('is_position')->after('file_grid')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_screens', function (Blueprint $table) {
            $table->dropColumn('grid');
            $table->dropColumn('file_grid');
            $table->dropColumn('is_position');
        });
    }
}
