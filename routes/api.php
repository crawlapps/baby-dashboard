<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('user/getKey', 'Api\User\UserApicontroller@getAuthorizationKey');

Route::group(['middleware' => ['key']], function () {

    Route::post('user/add-user', 'Api\User\UserApicontroller@register');
    Route::post('user/getnewtoken', 'Api\User\UserApicontroller@getnewtoken');

    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('relation/getrelation', 'Api\Relation\RelationApicontroller@index');
        Route::get('homescreen/gethomescreen', 'Api\HomeScreen\HomeScreenApicontroller@index');
        Route::get('poster/getposter', 'Api\Poster\PosterApicontroller@index');
        Route::get('sticker/getsticker', 'Api\Category\CategoryApicontroller@index');
        Route::get('collage/getcollage', 'Api\Collage\CollageApicontroller@index');
        Route::get('sticker/getbackground', 'Api\Background\BackgroundApiController@index');

        Route::get('relation/getrelationfromtype', 'Api\Relation\RelationApicontroller@getrelationfromtype');
        Route::get('relation/getallimagesofrelation', 'Api\Relation\RelationApicontroller@getallimagesofrelation');

        Route::get('homescreen/getallhomescreenimage', 'Api\HomeScreen\HomeScreenApicontroller@getAllHomescreenImage');

        Route::get('single/getsingleimagecategory', 'Api\Single\SingleCategoryController@getSingleImageCategory');
        Route::get('single/getsingleimagehomescreen', 'Api\Single\SingleCategoryController@getSingleImageHomescreen');

        Route::get('collage/getcollagecategory', 'Api\Collage\CollageCategoryController@getCollageCategory');
        Route::get('collage/getcollagehomescreen', 'Api\Collage\CollageCategoryController@getCollageHomescreen');

        Route::post('user/sendfeedback', 'Api\User\UserApicontroller@sendFeedbackMail');

        Route::get('font/getfont', 'Api\Font\FontApiController@index');
    });
});

Route::get('test/getdata', 'Api\Test\TestController@index');

