<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', function () {
//    return view('auth.login');
//});

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('google/setauth', 'Google\GoogleController@setAuth')->name('setauth');
Route::get('google/addworksheet', 'Google\GoogleController@addWorkSheet')->name('addworksheet');

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('relation');

    /**
     * single category routes
     */
    Route::group(['namespace' => 'Single'], function(){
        Route::resource('singlecategory', 'SingleCategoryController');
    });
    Route::post('singlecategory/change_status/{id}','Single\SingleCategoryController@change_status');
    Route::post('singlecategory/re-order', 'Single\SingleCategoryController@reOrder')->name('singlecategory.reorder');
    /**
     * collage category routes
     */
    Route::group(['namespace' => 'Collage'], function(){
        Route::resource('collagecategory', 'CollageCategoryController');
    });
    Route::get('collagehomescreen','HomeScreen\HomeScreenController@collageHomescreen')->name('collagehomescreen');
    Route::post('collagecategory/change_status/{id}','Collage\CollageCategoryController@change_status');
    Route::get('collageelements','Collage\CollageCategoryController@collageElements')->name('collageelements');
    Route::post('collageelements/re-order', 'Collage\CollageCategoryController@reOrder')->name('collageelements.reorder');
    Route::post('collagecategory/category-re-order', 'Collage\CollageCategoryController@categoryreOrder')->name('collagecategory.categoryreorder');

    Route::get('singlehomescreen','Single\SingleCategoryController@singleHomescreen')->name('singlehomescreen');


        /**
     * relation routes
     */
    Route::group(['namespace' => 'Relation'], function(){
        Route::resource('relation', 'RelationController');
    });
    Route::get('homescreen_relation','Relation\RelationController@homescreen_relation')->name('homescreen_relation');
    Route::post('relation/change_status/{id}','Relation\RelationController@change_status');

    Route::post('relation_image/re-order', 'Relation\RelationController@reOrder')->name('relation_image.reorder');
    /**
     * relation Type routes
     */
    Route::group(['namespace' => 'Relation'], function(){
        Route::resource('relationtype', 'RelationTypeController');
    });
    Route::post('relationtype/change_status/{id}','Relation\RelationTypeController@change_status');
    Route::post('relationtype/re-order', 'Relation\RelationTypeController@reOrder')->name('relationtype.reorder');
    /**
     * home Screen routes
     */
    Route::group(['namespace' => 'HomeScreen'], function(){
        Route::resource('homescreen', 'HomeScreenController');
    });

    Route::get('homescreen_type/{type}','HomeScreen\HomeScreenController@typeindex')->name('homescreen_type');
    Route::post('homescreen/change_activestatus/{id}','HomeScreen\HomeScreenController@change_activestatus');
    Route::post('homescreen/change_status/{id}','HomeScreen\HomeScreenController@change_status');
    Route::post('homescreen/is_pro', 'HomeScreen\HomeScreenController@setIsPro')->name('homescreen.set_is_pro');
    Route::post('homescreen/re-order', 'HomeScreen\HomeScreenController@reOrder')->name('homescreen.reorder');

    /**
     * Posters routes
     */
    Route::group(['namespace' => 'Posters'], function(){
        Route::resource('poster', 'PosterController');
    });

    Route::post('poster/change_status/{id}','Posters\PosterController@change_status');
    Route::post('poster/is_pro', 'Posters\PosterController@setIsPro')->name('poster.set_is_pro');
    Route::post('poster/re-order', 'Posters\PosterController@reOrder')->name('poster.reorder');
    /**
     * Stickers routes
     */
    Route::group(['namespace' => 'Sticker'], function(){
        Route::resource('sticker', 'StickerController');
    });
    Route::post('sticker/change_status/{id}','Sticker\StickerController@change_status');
    Route::post('sticker/is_pro', 'Sticker\StickerController@setIsPro')->name('sticker.set_is_pro');
    Route::post('sticker/is_color', 'Sticker\StickerController@setIsColor')->name('sticker.set_is_color');
    Route::post('sticker/re-order', 'Sticker\StickerController@reOrder')->name('sticker.reorder');
    /**
     * Category routes
     */
    Route::group(['namespace' => 'Category'], function(){
        Route::resource('category', 'CategoryController');
    });
    Route::post('category/re-order', 'Category\CategoryController@reOrder')->name('category.reorder');
    Route::post('category/change_lock/{id}','Category\CategoryController@change_lock');

    /**
     * milestone routes
     */
    Route::get('milestone','Posters\PosterController@milestone_index')->name('milestone');

    /**
     * background category routes
     */
    Route::group(['namespace' => 'Background'], function(){
        Route::resource('backgroundcategory', 'BackgorundCategoryController');
    });
    Route::post('backgroundcategory/change_status/{id}','Background\BackgorundCategoryController@change_status');
    Route::post('backgroundcategory/re-order', 'Background\BackgorundCategoryController@reOrder')->name('backgroundcategory.reorder');

    /**
     * background routes
     */

    Route::group(['namespace' => 'Background'], function(){
        Route::resource('background', 'BackgorundController');
    });

    Route::post('background/change_status/{id}','Background\BackgorundController@change_status');
    Route::post('background/is_pro', 'Background\BackgorundController@setIsPro')->name('background.set_is_pro');
    Route::post('background/re-order', 'Background\BackgorundController@reOrder')->name('background.reorder');

    /**
     * Key routes
     */
    Route::group(['namespace' => 'Setting'], function(){
        Route::resource('setting', 'SettingController');
    });
    Route::post('setting/change_status/{id}','Setting\SettingController@change_status');

    /**
     * Font routes
     */
    Route::group(['namespace' => 'Font'], function(){
        Route::resource('font', 'FontController');
    });
    Route::post('font/change_status/{id}','Font\FontController@change_status');
    Route::post('font/re-order', 'Font\FontController@reOrder')->name('font.reorder');
    Route::post('font/is_pro', 'Font\FontController@setIsPro')->name('font.set_is_pro');
});
