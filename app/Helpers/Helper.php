<?php

if (!function_exists('convert_char_UTF_8')) {
    /**
     * @param $str
     * @return bool|false|string
     */
    function convert_char_UTF_8($str)
    {
        return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $str);
    }
}
