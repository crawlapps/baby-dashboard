<?php

namespace App\Traits;

use App\Http\Controllers\Controller;
use Google\Spreadsheet\SpreadsheetService;
use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_Spreadsheet;
use Illuminate\Http\Request;

trait PasswordTrait
{
    private $client;
    private $tokenPath;
    private $spreadsheetId;
    private $range;
    private $service;

    /*
         * BEFORE RUNNING:
         * ---------------
         * 1. If not already done, enable the Google Sheets API
         *    and check the quota for your project at
         *    https://console.developers.google.com/apis/api/sheets
         * 2. Install the PHP client library with Composer. Check installation
         *    instructions at https://github.com/google/google-api-php-client.
     */
    public function createSheet()
    {
        $client = $this->getClient();
        $service = new Google_Service_Sheets($this->client);
//        dd(get_class_methods($service));
        $requestBody = new Google_Service_Sheets_Spreadsheet(array('properties' => [
                'title' => 'crawlapps_credentials_dashboard'])
        );
        $response = $service->spreadsheets->create($requestBody);
        dd($response);
//        echo '<pre>', var_export($response, true), '</pre>', "\n";
    }
    public function createWorkSheet()
    {
        try {
            $client = $this->getClient();
            $this->service = new Google_Service_Sheets($this->client);
            $this->spreadsheetId = env('SPREADSHEET_ID');
            $range = env('WORKSHEET_NAME');

            $sheetInfo = $this->service->spreadsheets->get($this->spreadsheetId);
            $allsheet_info = $sheetInfo['sheets'];
            $idCats = array_column($allsheet_info, 'properties');

            if ($this->is_sheet_available($idCats, $range)) {
                dd('Sheet already exist');
            } else {
                $requestBody = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest(
                    array(
                        'requests' => array('addSheet' => array('properties' => array('title' => $range))))
                );
                $this->service->spreadsheets->batchUpdate($this->spreadsheetId, $requestBody);

                $value = [
                    ['Email', 'Passoword', 'Last changed time'],
                ];

                $response = $this->updateValues($range, $value);
                dd($response);
            }

        } catch (\Exception $e) {
            dd($e);
        }
    }
    public function is_sheet_available(array $mySheets, $new_sheet) {
        foreach ($mySheets as $element) {
            if ($element->title == $new_sheet) {
                return true;
            }
        }
        return false;
    }

    public function addPassword( $email ){
        $client = $this->getClient();
        $this->service = new Google_Service_Sheets($this->client);
        $this->spreadsheetId = env('SPREADSHEET_ID');
        $range = env('WORKSHEET_NAME') . '!A2:C2';
        $org = $this->generatePassword();
        $currentDateTime = date('y:m:d h:i:s');
        $hash = password_hash($org, PASSWORD_BCRYPT);
        $value = [
            [ $email ,$org, $currentDateTime],
        ];
        $response = $this->updateValues($range, $value);
        return $hash;
    }
    public function generatePassword()
    {
        $lc = range('a', 'z');
        $uc = range('A', 'Z');
        $digits =  range(0,20);
        $spe_char = ['$', '#', '@', '!', '.', '-','$', '#', '@', '!', '.', '-' ];

        shuffle($lc);
        shuffle($uc);
        shuffle($digits);
        shuffle($spe_char);

        $LC = array_slice($lc,5,4);
        $UC = array_slice($uc,5,4);
        $SPE_CHAR = array_slice($spe_char,3,5);
        $DIGITS = array_slice($digits,5,5);

        $SPE_LC = array_merge($LC, $SPE_CHAR);
        $DIG_UC = array_merge( $UC, $DIGITS);

        shuffle($SPE_LC);
        shuffle($DIG_UC);

        $characters = array_merge( $SPE_LC, $DIG_UC );
        shuffle($characters);
        $org = implode('',$characters);
        return $org;
    }

    public function updateValues($range, $value){
        $body = new \Google_Service_Sheets_ValueRange([
            "values" => $value
        ]);
        $params =   [
            'valueInputOption' => 'RAW'
        ];
        $insert = [
            'insertDataOption' => 'INSERT_ROWS'
        ];
        $response = $this->service->spreadsheets_values->update($this->spreadsheetId, $range, $body, $params, $insert);
    }
    public function getClient()
    {
        $this->set_client();

        if (file_exists($this->tokenPath)) {
            $accessToken = json_decode(file_get_contents($this->tokenPath), true);
            $this->client->setAccessToken($accessToken);
        }
        // If there is no previous token or it's expired.
        if ($this->client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($this->client->getRefreshToken()) {
                $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $this->client->createAuthUrl();
                echo redirect($authUrl);
            }

        }
    }

    public function setAuth(Request $request)
    {
        if (isset($request->code)) {
            $this->set_client();

            $accessToken = $this->client->fetchAccessTokenWithAuthCode($request->code);
            $this->client->setAccessToken($accessToken);
            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
            // Save the token to a file.
            if (!file_exists(dirname($this->tokenPath))) {
                mkdir(dirname($this->tokenPath), 0700, true);
            }
            file_put_contents($this->tokenPath, json_encode($this->client->getAccessToken()));
        }
        return $this->client;
    }

    public function set_client()
    {
        $this->client = new Google_Client();
        $this->client->setRedirectUri(env('GOOGLE_REDIRECT_URI'));
        $this->client->setApplicationName(env('APP_NAME'));
        $this->client->setScopes(Google_Service_Sheets::SPREADSHEETS);
        $this->client->setAuthConfig(base_path() . '/google_secrets/credentials.json');
        $this->client->setAccessType('offline');
        $this->client->setPrompt('select_account consent');
        $this->tokenPath = public_path() . '/google_token/token.json';
    }
}
