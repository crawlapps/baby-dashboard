<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";

    public function sticker(){
        return $this->hasMany(Sticker::class,'category_id','id')->where('is_enable',1)->orderBy('is_pro', 'asc')->orderBy('order_by','asc');
    }
}
