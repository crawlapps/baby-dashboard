<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sticker extends Model
{
    use SoftDeletes;
    protected $table = "stickers";

    public function belongs_to_category(){
        return $this->belongsTo(Category::class , 'category_id', 'id');
    }
}
