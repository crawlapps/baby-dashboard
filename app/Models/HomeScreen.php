<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class   HomeScreen extends Model
{
    use SoftDeletes;
    protected $table = "home_screens";

    /**
     * Get the collages that contain the home_screen_id.
     */
    public function collage()
    {
        return $this->hasMany(Monthly_Collage::class,'home_screen_id','id')->orderBy('position');
    }
    /**
     * Get the relations that contain the home_screen_id.
     */
    public function relation()
    {
        return $this->hasMany(Relation::class,'home_screen_id','id')->orderBy('order_by','asc');
    }

}
