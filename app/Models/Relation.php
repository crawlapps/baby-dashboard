<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Relation extends Model
{
    use SoftDeletes;
    protected $table = "relations";

    public function RelationType(){
        return $this->belongsTo(RelationType::class,'relation_type_id','id');
    }
}
