<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollageCategory extends Model
{
    protected $table = "collage_categories";
}
