<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BackgroundCategory extends Model
{
    use SoftDeletes;
    protected $table = "background_categories";

    public function background(){
        return $this->hasMany(Background::class,'background_category_id','id')->where('is_active',1)->orderBy('is_pro','asc')->orderBy('order_by','desc');
    }

}
