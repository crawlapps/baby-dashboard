<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MonthlyPoster extends Model
{
    use SoftDeletes;
    protected $table = 'monthly_posters';

    public function month(){
        return $this->belongsTo(Month::class);
    }
}
