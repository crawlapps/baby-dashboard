<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    protected $table = "month";

    public function poster(){
        return $this->hasMany(MonthlyPoster::class,'month_id','id')->where('is_enable',1)->orderBy('is_pro', 'asc')->orderBy('created_at','desc');
    }
}
