<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationType extends Model
{
   protected $table = "relation_type";

    public function Relation(){
        return $this->hasMany(Relation::class,'relation_type_id','id')->where('is_enable',1)->orderBy('order_by','asc');
    }

    public function HomeScreen(){
        return $this->hasMany(HomeScreen::class,'relation_type_id','id')->where('is_active',1)->orderBy('is_pro', 'asc')->orderBy('order_by','desc');
    }
}
