<?php

namespace App\Http\Requests\HomeScreen;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

class HomeScreenRequest extends FormRequest
{
    public static $rules = [
        'title' => "required"
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::$rules;
//        dd($this::all());
        switch (Route::currentRouteName()) {
            case 'homescreen.store':
            {
                if( $this['type'] == '1' ){
                    $rules['file_homescreen'] = 'required|mimes:jpeg,bmp,png';
                    $rules['file_withouttransparent'] = 'required|mimes:jpeg,bmp,png';
                    $rules['x'] = 'required';
                    $rules['y'] = 'required';
                    $rules['height'] = 'required';
                    $rules['width'] = 'required';
                    $rules['ratio'] = 'required|regex:/^[0-9:]+$/u';
                }
                if( $this['type'] == '2' ){
                    $rules['file_homescreen'] = 'required|mimes:jpeg,bmp,png';
                    $rules['relation'] = 'required';
                    $rules['relation.*'] = 'mimes:jpeg,bmp,png';
                    $rules['relation_type_id'] = 'required';
                }
                if( $this['type'] == '3' ){
                    $rules['grid'] = 'required';
                    $rules['ratio'] = 'required|regex:/^[0-9:]+$/u';
                    $rules['file_grid'] = 'required|mimes:jpeg,bmp,png';
                    $rules['file_withoutbaby'] = 'required|mimes:jpeg,bmp,png';

                    if( is_array($this->position) ) {
                        foreach ($this->position as $i => $val) {
                            $rules['position.' . $i . '.*'] = 'required';
                        }
                    }
                }
                return $rules;
            }
            case 'homescreen.update':
            {
                if( $this['type'] == '1' ){
                    $rules['ratio'] = 'required|regex:/^[0-9:]+$/u';
                }
                if( $this['type'] == '3' ){
                    $rules['ratio'] = 'required|regex:/^[0-9:]+$/u';
                   if( is_array($this->position) ) {
                       foreach ($this->position as $i => $val) {
                           $rules['position.' . $i . '.*'] = 'required';
                       }
                   }
                }
                //dd($rules);
                return $rules;
            }

            default:
                break;
        }
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $rules =[
//            'title.*' => 'abc',
            //'type.*' => 'Home Screen Type is required..',
//            'collages.required' => 'Select images for all checked position...',
            'file_grid.*' => 'Grid file id required',
            'hdn_file_grid.*' => 'Grid file id required',
            'relation.0.mimes' => 'Relation field must be type of jpg/jpeg/png..',
            'relation_type_id.required' => 'Relation Name field is required',
            'ratio' => 'Ratio field required',
            'ratio.*' => 'Ratio only contains digit and colon',
        ];
        if( is_array($this->position) ) {
            foreach ($this->position as $i => $val) {
                $rules['position.' . $i . '.*'] = 'Fill all elements of position ' . $i;
            }
        }
        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
