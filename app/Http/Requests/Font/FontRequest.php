<?php

namespace App\Http\Requests\Font;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

class FontRequest extends FormRequest
{
    public static $rules = [
        'name' => 'required',
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::$rules;
        switch (Route::currentRouteName()) {
            case 'font.store':
            {
                $rules['file_font'] = 'required';
                return $rules;

            }
            case 'font.update':
            {
                return $rules;
            }

            default:
                break;
        }
    }
    public function messages()
    {
        return [
            'name.required' => 'Font Name is required...',
            'file_font.*' => 'Font file is required...'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
