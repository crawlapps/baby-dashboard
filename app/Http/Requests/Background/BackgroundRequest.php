<?php

namespace App\Http\Requests\Background;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

class BackgroundRequest extends FormRequest
{
    public static $rules = [
        'category' => "required",
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::$rules;

        switch (Route::currentRouteName()) {
            case 'background.store':
            {
                $rules['file_background'] = 'required';
                $rules['file_background.*'] = 'mimes:jpeg,bmp,png';
                return $rules;

            }
            case 'background.update':
            {
                $rules['file_background'] = 'mimes:jpeg,bmp,png';
                return $rules;
            }

            default:
                break;
        }
    }
    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mimes' => 'Background field must be type of jpg/jpeg/png..',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
