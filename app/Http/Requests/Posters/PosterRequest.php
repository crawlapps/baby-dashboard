<?php

namespace App\Http\Requests\Posters;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

class PosterRequest extends FormRequest
{
    public static $rules = [
        'month' => "required",
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::$rules;

        switch (Route::currentRouteName()) {
            case 'poster.store':
            {
                $rules['poster'] = 'required';
                $rules['poster.*'] = 'mimes:jpeg,bmp,png';
                $rules['file_posterwithbaby'] = 'required';
                $rules['file_posterwithbaby.*'] = 'mimes:jpeg,bmp,png';
                return $rules;

            }
            case 'poster.update':
            {
                $rules['poster'] = 'mimes:jpeg,bmp,png';
                $rules['file_posterwithbaby'] = 'mimes:jpeg,bmp,png';
                return $rules;
            }

            default:
                break;
        }
    }
    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'month.*' => 'Please Select Month..',
            'mimes' => 'Poster field must be type of jpg/jpeg/png..',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
