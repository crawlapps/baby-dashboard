<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BackgroundCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $background = $this->background->pluck('file_background')->map(function($name) {
            return  \Storage::disk('public')->url('uploads/background/'.$name);
        });
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => \Storage::disk('public')->url('uploads/backgroundcategory/'.$this->file_background_category),
            'stickers' => BackgroundResource::collection($this->background)
//            'images' =>  $background->toArray(),
        ];
    }
}
