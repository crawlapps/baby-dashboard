<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BackgroundResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'file' => ($this->file_background) ? asset('storage/uploads/background/') .'/'. $this->file_background : null,
            'is_pro' => ( $this->is_pro == 0 ) ? false : true
        ];
    }
}
