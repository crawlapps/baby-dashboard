<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HomeScreenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
//        dd($request, $request->getPathInfo());
        return [
            'id' => $this->id,
            'type' => $this->type,
            'title' => $this->title,

            $this->mergeWhen($this->type == "1" || $this->type == "2" , [
                'file_homescreen' => ($this->file_homescreen) ? asset('storage/uploads/homescreens/' . $this->file_homescreen) : '',
            ]),
            $this->mergeWhen($this->type == "1", [
//                'transparent' => ($this->file_homescreen) ? asset('storage/uploads/homescreens/' . $this->file_homescreen) : '',
                'without_transparent' => ($this->file_withouttransparent) ? asset('storage/uploads/homescreens/' . $this->file_withouttransparent) : '',
                'x' => $this->x,
                'y' => $this->y,
                'height' => $this->height,
                'width' => $this->width,
                'ratio' => $this->ratio,
                'rotation' => $this->rotation
            ]),
            $this->mergeWhen($this->type == "3", [
            'title' => $this->title,
            'file_homescreen' => ($this->file_grid) ? asset('storage/uploads/grid/' . $this->file_grid) : '',
            'without_transparent' => ($this->file_withouttransparent) ? asset('storage/uploads/homescreens/' . $this->file_withouttransparent) : '',
            'grid' => $this->grid,
            'ratio' => $this->ratio,
            'collage' => (@$this->collage) ? CollageResource::collection($this->collage) : '',
            ]),
            $this->mergeWhen(( request()->getPathInfo() == '/api/homescreen/gethomescreen'  && $this->type == "2") || ( request()->getPathInfo() == '/api/homescreen/getallhomescreenimage' &&  $this->type == "2") , [
                'relation' => (@$this->relation) ? RelationResource::collection($this->relation) : '',
            ]),
            'is_home' => ( $this->is_home == "1" ) ? true : false,
            'is_pro' => ( $this->is_pro == 0 ) ? false : true,
        ];
    }
}
