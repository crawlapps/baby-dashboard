<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PosterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'poster' => ($this->poster) ? asset('storage/uploads/poster/' . $this->poster) : '',
            'posterwithbaby' => ($this->file_posterwithbaby) ? asset('storage/uploads/poster/' . $this->file_posterwithbaby) : '',
            'is_pro' => ( $this->is_pro == 0 ) ? false : true
        ];
    }
}
