<?php

namespace App\Http\Resources;

use App\Models\Sticker;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $sticker = $this->sticker->pluck('file_sticker')->map(function($name) {
            return  \Storage::disk('public')->url('uploads/sticker/'.$name);
        });

        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => \Storage::disk('public')->url('uploads/category/'.$this->file_category),
//            'image' => asset('storage/uploads/category/'.$this->file_category),
//            'images' =>  $sticker->toArray(),
            'stickers' => StickerResource::collection($this->sticker)
        ];
    }
}
