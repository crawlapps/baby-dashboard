<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StickerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'file' => ($this->file_sticker) ? asset('storage/uploads/sticker/') .'/'. $this->file_sticker : null,
            'is_pro' => ( $this->is_pro == 0 ) ? false : true,
            'is_color' => ($this->is_color == 0 ) ? false : true,
            'color_code' => $this->color_code,
        ];
    }
}
