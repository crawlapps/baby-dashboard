<?php

namespace App\Http\Resources;

use Cassandra\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class RelationTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'relation_name' => $this->relation_name,
            'relation_images' => HomeScreenResource::Collection($this->homescreen),
//            'relation_images' => RelationResource::Collection($this->relation),
        ];
    }
}
