<?php

namespace App\Http\Resources;

use App\Models\MonthlyPoster;
use Illuminate\Http\Resources\Json\JsonResource;

class MonthResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'month' => $this->month,
//            'poster' => PosterResource::collection($this->poster)->forPage($request->page, 5),
            'poster' => PosterResource::collection(MonthlyPoster::where('month_id', $this->id)->where('is_enable', 1)->orderBy('is_pro', 'asc')->orderBy('order_by','desc')->paginate($request->perPage))->appends(null),

        ];
    }
}
