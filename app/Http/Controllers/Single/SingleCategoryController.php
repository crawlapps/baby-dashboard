<?php

namespace App\Http\Controllers\Single;

use App\Http\Controllers\Controller;
use App\Http\Requests\Single\SingleCategoryRequest;
use App\Models\HomeScreen;
use App\Models\SingleCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SingleCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api')){
            return Datatables::of(SingleCategory::orderBy('order_by','asc')->get())->make(true);
        }
        return view('pages.single.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $singlecategory = false;
        return view('pages.single.create', compact('singlecategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SingleCategoryRequest $request)
    {
        try {
            $category = new SingleCategory;
            $category->name = convert_char_UTF_8($request->name);
            $category->order_by = SingleCategory::count() + 1;

            $category->save();
            return redirect("singlecategory");
//            return redirect()->back()->with('success', 'RelationType Added successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'category not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SingleCategory $singlecategory)
    {
        return view('pages.single.create', compact('singlecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SingleCategoryRequest $request, SingleCategory $singlecategory)
    {
        try {
            $singlecategory->name = convert_char_UTF_8($request->name);

            $singlecategory->save();
            return redirect("singlecategory");
//            return redirect()->back()->with('success', 'RelationType Updated successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Category not Updated...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SingleCategory $singlecategory)
    {
        try {
            $singlecategory->delete();
            return response()->json(['data' => 'Category deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Category not Deleted...', 'status' => '0']);
        }
    }

    // redirect single category to single index balde

    public function singleHomescreen(Request $request){
        $category_id = $request->id;
        if ($request->filled('api')) {
            $entity = HomeScreen::where('type', '1')->where('single_category_id', $category_id)->orderBy('order_by', 'desc')->get();
            return Datatables::of($entity)->make(true);
        }

        $category= SingleCategory::select('id','name')->where('id',$category_id)->first();
        return view('pages.homescreen.single',compact('category_id','category'));
    }

    public function change_status($id)
    {
        try {
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $entity = SingleCategory::where('id', $id)->first();
            $entity->is_active = $value;

            \DB::table('home_screens')
                ->where('single_category_id', $id)
                ->update(['is_active' => $value]);

            if( $entity->save()){
                return response()->json(['data' => 'category Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'category Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'category Status not changed...', 'status' => '0']);
        }
    }
    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = SingleCategory::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }

}
