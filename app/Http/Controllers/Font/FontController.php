<?php

namespace App\Http\Controllers\Font;

use App\Http\Controllers\Controller;
use App\Http\Requests\Font\FontRequest;
use App\Models\Font;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class FontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api')){
            return Datatables::of(Font::orderBy('order_by','asc')->get())->make(true);
        }
        return view('pages.font.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $font = false;
        return view('pages.font.create', compact('font'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FontRequest $request)
    {
        try {
            $font = new Font;
            $file = $request->file('file_font');
            $destinationPath = public_path('/uploads/font/');
            $fileName = $file->getClientOriginalName();
            $file->move($destinationPath, $fileName);
//            $file = ImageTrait::makeImage($request->file_font, 'uploads/font/');

            $font->name = convert_char_UTF_8($request->name);
            $font->file_font = $fileName;
            $font->order_by = Font::count() + 1;
            $font->save();

            return redirect('font');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Font not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(font $font)
    {
        return view('pages.font.create', compact('font'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FontRequest $request, Font $font)
    {
        try {
            if( $request->file_font ) {
                $file = $request->file('file_font');
                $destinationPath = public_path('/uploads/font/');
                $fileName = $file->getClientOriginalName();
                $file->move($destinationPath, $fileName);

                $path = public_path('/uploads/font/'.$font->file_font);
                if(file_exists($path)){
                    unlink($path);
                }
//                $file = ImageTrait::makeImage($request->file_font, public_path('uploads/font/'));
            }else{
                $fileName = $font->file_font;
            }

            $font->name = convert_char_UTF_8($request->name);
            $font->file_font = $fileName;
            $font->save();

            return redirect('font');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Font not added...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Font $font)
    {
        try {
            $path = public_path('/uploads/font/'.$font->file_font);
            if(file_exists($path)){
                unlink($path);
            }
            $font->delete();

            return response()->json(['data' => 'Font deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Font not Deleted...', 'status' => '0']);
        }
    }

    public function change_status($id)
    {
        try {
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $entity = Font::where('id', $id)->first();
            $entity->is_active = $value;

            if( $entity->save()){
                return response()->json(['data' => 'font Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'font Status not changed...', 'status' => '0']);
            }
        } catch (Exception $e) {
            return response()->json(['data' => 'font Status not changed...', 'status' => '0']);
        }
    }

    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = Font::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }

    public function setIsPro(Request $request)
    {
        try {
            $pro = $request->is_pro ? 0 : 1;
            $sticker = Font::where('id', $request->id)->first();
            $sticker->is_pro = $pro;

            if( $sticker->save()){
                return response()->json(['data' => 'Font Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Font Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Font Status not changed...', 'status' => '0']);
        }
    }
}
