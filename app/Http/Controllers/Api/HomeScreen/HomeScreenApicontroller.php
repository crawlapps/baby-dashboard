<?php

namespace App\Http\Controllers\Api\HomeScreen;

use App\Http\Controllers\Controller;
use App\Http\Resources\HomeScreenResource;
use App\Http\Resources\RelationResource;
use App\Models\HomeScreen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class HomeScreenApicontroller extends Controller
{
    public function index(Request $request)
    {
            try {
                $homescreen = HomeScreen::with('collage')->with('relation')->where('is_active', 1)->inRandomOrder()->paginate($request->per_page)->appends(null);
                $screen = $homescreen->shuffle();
                $homescreen = HomeScreenResource::collection($homescreen);
                return response()->json(['data' => $homescreen], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "home Screen not found..."], 422);
            }
    }

    public function getAllHomescreenImage(Request $request)
    {
            try {
                $page = $request->page;
                $total = (($page - 1) * 12);
                $singlelimit = $total - (($page - 1) * 4);
                $relationlimit = ($page > 1) ? ($total / 6) : 0;

                $entity = collect();
                $total_record['single'] = number_format(ceil(HomeScreen::where('is_home', 1)->where('is_active', 1)->where('type', 1)->count() / 8), 0);
                $total_record['relation'] = number_format(ceil(HomeScreen::where('is_home', 1)->where('is_active', 1)->where('type', 2)->count() / 2), 0);
                $total_record['collage'] = number_format(ceil(HomeScreen::where('is_home', 1)->where('is_active', 1)->where('type', 3)->count() / 2), 0);

                $total_page = max($total_record);

                for ($i = 0; $i < 2; $i++) {
                    $single = HomeScreen::where('is_home', 1)->where('type', 1)->where('is_active', 1)->limit(4)->offset($singlelimit)->orderBy('updated_at', 'desc')->get();

                    $entity = $entity->merge($single);

                    $relation = HomeScreen::with('relation')->where('is_home', 1)->where('is_active', 1)->where('type', 2)->inRandomOrder()->limit(1)->offset($relationlimit)->get();

                    $entity = $entity->merge($relation);
//
                    $collage = HomeScreen::with('collage')->where('is_home', 1)->where('is_active', 1)->where('type', 3)->inRandomOrder()->limit(1)->offset($relationlimit)->get();

                    $entity = $entity->merge($collage);

                    $singlelimit = $singlelimit + 4;
                    $relationlimit = $relationlimit + 1;
                }
                $homescreen = HomeScreenResource::collection($entity);
                $number = random_int(1,5);

                $data['data'] = $homescreen;
                $data['data'] = array_merge($data, ['current_page' => $page, 'total_page' => $total_page, 'number' => $number]);

                return response()->json($data, 200);

//                $cacheKey = 'homescreen';
//                Redis::del($cacheKey);
//                Redis::set($cacheKey, json_encode($data));
//
//                if (\Request::is('api*')) {
//                    return response()->json([
//                        "status" => true,
//                        "message" => "Category list get successfully.",
//                        'data' => (!empty(Redis::get($cacheKey))) ? json_decode(Redis::get($cacheKey)) : $data
//                    ], 200);
//                }
//                return json_encode($data);

            } catch
            (Exception $e) {
                return response()->json(['message' => "home Screen not found..."], 422);
            }
    }

//    public function getAllHomescreenImage(){
//        try {
//            $homescreen = HomeScreen::with('collage')->with('relation')->where('is_home',1)->orderBy('created_at','desc');
//
//            $homescreen = HomeScreenResource::collection($homescreen);
//            return response()->json(['data' => $homescreen],200);
//        } catch
//        (Exception $e) {
//            return response()->json(['data' => "home Screen not found..."] , 422);
//        }
//    }
}
