<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Models\Sticker;
use Illuminate\Http\Request;

class CategoryApicontroller extends Controller
{
    public function index(Request $request)
    {
        try {
            $sticker = Category::with('sticker')->where('is_lock', '1')->orderBy('order_by', 'asc')->get();
            $sticker = CategoryResource::collection($sticker);
            return response()->json(['data' => $sticker], 200);
        } catch
        (Exception $e) {
            return response()->json(['message' => "Sticker not found..."], 422);
        }
    }
}
