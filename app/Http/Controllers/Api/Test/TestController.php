<?php

namespace App\Http\Controllers\Api\Test;

use App\Http\Controllers\Controller;
use App\Http\Resources\HomeScreenResource;
use App\Models\HomeScreen;
use Illuminate\Http\Request;
use phpseclib\Crypt\Random;

class TestController extends Controller
{
    public function index(){
        try {
            for( $i=0; $i<20; $i++ ){
                $num = random_int(1,100);
                $data[$i]['title'] = 'title' . $num;
                $data[$i]['value'] = $num;
            }

            return response()->json(['data' => $data], 200);
        } catch
        (Exception $e) {
            return response()->json(['message' => "Home Screen not found..."], 422);
        }
    }

}
