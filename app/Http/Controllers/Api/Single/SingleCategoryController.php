<?php

namespace App\Http\Controllers\Api\Single;

use App\Http\Controllers\Controller;
use App\Http\Resources\HomeScreenResource;
use App\Http\Resources\SingleCategoryResource;
use App\Models\HomeScreen;
use App\Models\SingleCategory;
use Illuminate\Http\Request;

class SingleCategoryController extends Controller
{
    public function getSingleImageCategory(Request $request)
    {
            try {
                $category = SingleCategory::where('is_active', 1)->orderBy('order_by', 'asc')->get();

                return response()->json(['data' => SingleCategoryResource::collection($category)], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Category not found..."], 422);
            }
    }

    public function getSingleImageHomescreen(Request $request)
    {
            try {
                $entity = HomeScreen::where('single_category_id', $request->category_id)->where('is_active', 1)->orderBy('is_pro', 'asc')->orderBy('order_by', 'desc')->get();

                return response()->json(['data' => HomeScreenResource::collection($entity)], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Home Screen not found..."], 422);
            }
    }
}
