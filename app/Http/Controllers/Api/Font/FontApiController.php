<?php

namespace App\Http\Controllers\Api\Font;

use App\Http\Controllers\Controller;
use App\Http\Resources\FontResource;
use App\Models\Font;
use Illuminate\Http\Request;

class FontApiController extends Controller
{
    public function index(Request $request)
    {
        try {
            $entity = Font::where('is_active', '1')->orderBy('is_pro', 'asc')->orderBy('order_by', 'asc')->paginate(15);
            $entity = FontResource::collection($entity)->appends(null);
            return response()->json(['data' => $entity], 200);
        } catch
        (Exception $e) {
            return response()->json(['message' => "Sticker not found..."], 422);
        }
    }
}
