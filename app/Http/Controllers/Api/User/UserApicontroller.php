<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserApicontroller extends Controller
{
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function getnewtoken(Request $request)
    {
            try {
                $refresh_token = $request->refresh_token;

                if (empty($refresh_token)) {
                    return response()->json(['message' => "Refresh token required..."], 422);
                }

                $user = User::where('refresh_token', $refresh_token)->first();
                if ($user) {
                    // $user->name is know as password
                    $data = $this->get_token($user->email, $user->name);

                    $user->access_token = $data->access_token;
                    $user->refresh_token = $data->refresh_token;
                    $user->save();

                    $data->access_token = $user->access_token;
                    $data->refresh_token = $user->refresh_token;
                    return response()->json(['data' => $data], 200);
                } else {
                    return response()->json(['message' => "Device not found or token not match...!"], 422);
                }
            } catch
            (Exception $e) {
                return response()->json(['message' => "Device not found..."], 422);
            }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
            try {
//        dd(env('PASSPORT_CLIENT_ID'));
                $validator = Validator::make($request->all(), [
                    'device_id' => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json(['message' => "Device id required..."], 422);
                }
                $email = 'user' . date('Ymdhis') . '@crawlapps.com';

                $isUser = User::where('name', $request->device_id)->first();

                $user = ($isUser) ? $isUser : new User;

                $user->name = $request->device_id;
                $user->email = $email;
                $user->password = bcrypt($request->device_id);

                if( !$isUser ){
                    $user->user_create_date = date('Y-m-d H:i:s');
                }
                $user->save();

                $data = $this->get_token($email, $request->device_id);

                if (!empty($data->error)) {
                    return response()->json(['message' => "Token not Generated..."], 422);
                }
                $user->access_token = $data->access_token;
                $user->refresh_token = $data->refresh_token;
                $user->save();

                $data->access_token = $user->access_token;
                $data->refresh_token = $user->refresh_token;
                $data->date = ($user->user_create_date) ? $user->user_create_date : null;

                return response()->json(['data' => $data], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Device not Registered..."], 422);
            }
    }

    /**
     * token generate
     * */
    public function get_token($email, $password)
    {
        $data = [
            'grant_type' => 'password',
//            'client_id' => 2,
//            'client_secret' => 'uhgIpe8XqM6QQ37R3h3Rx1gZI0qEG7EuDuFpvJFo',
            'client_id' => env('PASSPORT_CLIENT_ID'),
            'client_secret' => env('PASSPORT_CLIENT_SECRET'),
            'username' => $email,
            'password' => $password,
        ];
//        dd($data);
        $response = Request::create('/oauth/token', 'POST', $data);
        $response = app()->handle($response);
//        dd($response);
        // Get the data from the response
        $data = json_decode($response->getContent());
//        dd($data);
        return $data;
    }

    public function getAuthorizationKey()
    {
        try {
            $key = Setting::select('key')->where('is_active', 1)->first();

            $key = base64_encode($key->key);
            return response()->json(['data' => $key], 200);
        } catch
        (Exception $e) {
            return response()->json(['message' => "Key not found..."], 422);
        }
    }

    public function sendFeedbackMail(Request $request)
    {
        try {
            $data = array('data' => $request->message);
            Mail::send('pages.feedback.mail', $data, function ($message) {
                $message->to("ruchita.crawlapps@gmail.com", 'Feedback');
                $message->subject('First smile Feedback Message');
            });
            return response()->json(['data' => "Mail send successfully..."], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }

    }
}
