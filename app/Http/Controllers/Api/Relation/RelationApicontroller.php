<?php

namespace App\Http\Controllers\Api\Relation;

use App\Http\Controllers\Controller;
use App\Http\Resources\HomeScreenResource;
use App\Http\Resources\RelationResource;
use App\Http\Resources\RelationTypeResource;
use App\Models\HomeScreen;
use App\Models\RelationType;
use Illuminate\Http\Request;
use App\Models\Relation;
use Exception;

class RelationApicontroller extends Controller
{
    public function index(Request $request)
    {
            try {
                $relation = RelationType::with('homescreen')->where('is_active', 1)->orderBy('order_by', 'asc')->get();
                $relation = RelationTypeResource::collection($relation);
                return response()->json(['data' => $relation], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Relation not found..."], 422);
            }
    }

    public function getrelationfromtype(Request $request)
    {
            try {
                if ($request->relation_type_id == "") {
                    return response()->json(['message' => "Relation Type id not found..."], 422);
                }
                $relation_type = $request->relation_type_id;
                $homescreen = HomeScreen::where('relation_type_id', $relation_type)->where('is_active', 1)->orderBy('is_pro', 'asc')->orderBy('order_by', 'desc')->get();
                $homescreen = HomeScreenResource::collection($homescreen);
                return response()->json(['data' => $homescreen], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Relation not found..."], 422);
            }
    }

    public function getallimagesofrelation(Request $request)
    {
            try {
                if ($request->home_screen_id == "") {
                    return response()->json(['message' => "Home screen id not found..."], 422);
                }
                $home_screen_id = $request->home_screen_id;
                $relation = Relation::where('home_screen_id', $home_screen_id)->orderBy('order_by', 'asc')->get();
                $relation = relationResource::collection($relation);
                return response()->json(['data' => $relation], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Relation not found..."], 422);
            }
    }
}
