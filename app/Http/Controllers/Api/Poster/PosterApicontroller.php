<?php

namespace App\Http\Controllers\Api\Poster;

use App\Http\Controllers\Controller;
use App\Http\Resources\MonthResource;
use App\Http\Resources\PosterResource;
use App\Models\Month;
use App\Models\MonthlyPoster;
use Illuminate\Http\Request;

class PosterApicontroller extends Controller
{
    public function index(Request $request)
    {
            try {
                $month = Month::with('poster')->orderBy('created_at', 'asc')->get();
                $poster = MonthResource::collection($month);
                return response()->json(['data' => $poster], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Poster not found..."], 422);
            }
    }
}
