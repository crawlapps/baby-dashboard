<?php

namespace App\Http\Controllers\Api\Collage;

use App\Http\Controllers\Controller;
use App\Http\Resources\CollageCategoryResource;
use App\Http\Resources\HomeScreenResource;
use App\Models\CollageCategory;
use App\Models\HomeScreen;
use Illuminate\Http\Request;

class CollageCategoryController extends Controller
{
    public function getCollageCategory(Request $request)
    {
            try {
                $category = CollageCategory::orderBy('order_by', 'desc')->where('is_active', 1)->get();

                return response()->json(['data' => CollageCategoryResource::collection($category)], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Collage category not found..."], 422);
            }
    }

    public function getCollageHomescreen()
    {
            try {
                $entity = HomeScreen::where('type', 3)->where('is_active', 1)->orderBy('order_by', 'desc')->get();

                return response()->json(['data' => HomeScreenResource::collection($entity)], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Homescreen not found..."], 422);
            }
    }
}
