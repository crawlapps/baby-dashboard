<?php

namespace App\Http\Controllers\Api\Collage;

use App\Http\Controllers\Controller;
use App\Http\Resources\CollageResource;
use App\Http\Resources\HomeScreenResource;
use App\Models\HomeScreen;
use App\Models\Monthly_Collage;
use Illuminate\Http\Request;

class collageApicontroller extends Controller
{
//   public function index(Request $request){
//       try {
//           if( $request->home_screen_id == "" ){
//               return response()->json(['data' => "Enter HomeScreen id..."],422);
//           }
//           $collage = Monthly_Collage::select('id','position','image')->where('home_screen_id', $request->home_screen_id)->get();
//           $collage = CollageResource::collection($collage);
//           return response()->json(['data' => $collage],200);
//       } catch
//       (Exception $e) {
//           return response()->json(['data' => "Collages not found..."] , 422);
//       }
//   }

    public function index(Request $request)
    {
            try {
                $collage = HomeScreen::with('collage')->where('is_active', 1)->where('type', 3)->orderBy('is_pro', 'asc')->orderBy('order_by', 'desc')->get();
                $collage = HomeScreenResource::collection($collage);
                return response()->json(['data' => $collage], 200);
            } catch
            (Exception $e) {
                return response()->json(['message' => "Collages not found..."], 422);
            }
    }
}

