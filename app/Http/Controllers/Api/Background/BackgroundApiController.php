<?php

namespace App\Http\Controllers\Api\Background;

use App\Http\Controllers\Controller;
use App\Http\Resources\BackgroundCategoryResource;
use App\Models\BackgroundCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class BackgroundApiController extends Controller
{
    public function index()
    {
        try {
            // cache key = env('CACHE_PREFIX') . categories
            $cacheKey = 'background';
            $entity = BackgroundCategory::with('background')->where('is_active', 1)->orderBy('order_by', 'asc')->get();
            $entity = BackgroundCategoryResource::collection($entity);

            return response()->json(['data' =>$entity] , 200);
//            Redis::del($cacheKey);
//            Redis::set($cacheKey, json_encode($entity));
//
//            if (\Request::is('api*')) {
//                return response()->json([
//                    "status" => true,
//                    "message" => "Category list get successfully.",
//                    'data' => (!empty(Redis::get($cacheKey))) ? json_decode(Redis::get($cacheKey)) : $entity
//                ], 200);
//            }
//            return json_encode($entity);
        } catch
        (Exception $e) {
            return response()->json(['message' => "Background not found..."], 422);
        }
    }
}
