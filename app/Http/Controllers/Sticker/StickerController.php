<?php

namespace App\Http\Controllers\Sticker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sticker\StickerRequest;
use App\Models\Category;
use App\Models\MonthlyPoster;
use App\Models\Sticker;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class StickerController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category_id = $request->category_id;

        if($request->filled('api')){
            $sticker = Sticker::where('category_id',$category_id)->orderBy('order_by','asc')->get();
            return Datatables::of($sticker)->make(true);
        }

        $category = Category::select('name')->where('id',$category_id)->first();
        $name = $category->name;
        return view('pages.sticker.index', compact('category_id'),compact('name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $category = Category::where('id',$request->category_id)->first();
        $sticker = false;
        return view('pages.sticker.create',compact('sticker'),compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StickerRequest $request)
    {
        try {
            if ($request->hasFile('sticker')) {
                foreach($request->sticker as $image)
                {
                    $sticker = new Sticker;
                    $image = ImageTrait::makeImage($image, 'uploads/sticker/');

                    // for compress file
                    $this->destinationPath = public_path('/storage/uploads/sticker/');
                    $this->thumbnailDestinationPath = public_path('/storage/uploads/sticker/thumbnails/');
                    if (!file_exists( $this->thumbnailDestinationPath )) {
                        mkdir($this->thumbnailDestinationPath, 0777, true);
                    }
                    $this->compress_image($this->destinationPath . $image, $this->thumbnailDestinationPath . $image, env('QUALITY'));

                    $sticker->category_id = $request->category;
                    $sticker->file_sticker = $image;
                    $sticker->order_by = Sticker::count() + 1;
                    $sticker->color_code = "#".str_replace('#', '',$request->color_code);
                    $sticker->save();
                }
                return redirect('sticker'.'?category_id='.$sticker->category_id);
//                return redirect()->back()->with('success', 'Sticker Added successfully...');
            }else{
                return redirect()->back()->with('error', 'Sticker not added...');
            }


        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Sticker not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sticker $sticker)
    {
//        $categories = Category::get();
        $category = Category::where('id',$sticker->category_id)->first();
        return view('pages.sticker.create',compact('sticker'),compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StickerRequest $request, Sticker $sticker)
    {
        try {
            if ($request->hasFile('sticker')) {
                $image = ImageTrait::makeImage($request->sticker, 'uploads/sticker/');

                // for compress file
                $destinationPath = public_path('/storage/uploads/sticker/');
                $thumbnailDestinationPath = public_path('/storage/uploads/sticker/thumbnails/');
                if (!file_exists( $thumbnailDestinationPath )) {
                    mkdir($thumbnailDestinationPath, 0777, true);
                }
                $this->compress_image($destinationPath . $image, $thumbnailDestinationPath . $image, env('QUALITY'));
            }else{
                $image = $sticker->file_sticker;
            }
                $sticker->category_id = $request->category;
                $sticker->file_sticker = $image;
                $sticker->color_code = "#".str_replace('#', '',$request->color_code);
                $sticker->save();
                return redirect('sticker'.'?category_id='.$sticker->category_id);
//                return redirect()->back()->with('success', 'Sticker updated successfully...');


        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Sticker not update    ...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sticker $sticker)
    {
        try {
            $path = \Storage::path('uploads/sticker/'.$sticker->file_sticker);
            if(file_exists($path)){
                unlink($path);
            }
            $sticker->delete();
            return response()->json(['data' => 'Sticker deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Sticker not Deleted...', 'status' => '0']);
        }
    }

    /**
 * change the status of sticker.
 *
 * @param int $id
 * @return \Illuminate\Http\Response
 */
    public function change_status($id)
    {
        try {
            $sticker = Sticker::where('id', $id)->first();
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $sticker->category_id = $sticker->category_id;
            $sticker->file_sticker = $sticker->file_sticker;
            $sticker->is_enable = $value;
            if( $sticker->save()){
                return response()->json(['data' => 'Sticker Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Sticker Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Sticker Status not changed...', 'status' => '0']);
        }
    }

    public function setIsPro(Request $request)
    {
        try {
            $pro = $request->is_pro ? 0 : 1;
            $sticker = Sticker::where('id', $request->id)->first();
            $sticker->is_pro = $pro;
            if( $sticker->save()){
                return response()->json(['data' => 'Sticker Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Sticker Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Sticker Status not changed...', 'status' => '0']);
        }
    }

    public function setIsColor(Request $request)
    {
        try {
            $color = $request->is_color ? 0 : 1;
            $sticker = Sticker::where('id', $request->id)->first();
            $sticker->is_color = $color;
            if( $sticker->save()){
                return response()->json(['data' => 'Sticker Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Sticker Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Sticker Status not changed...', 'status' => '0']);
        }
    }

    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = Sticker::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }
}
