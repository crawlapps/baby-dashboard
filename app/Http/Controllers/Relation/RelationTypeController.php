<?php

namespace App\Http\Controllers\Relation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Relation\RelationTypeRequest;
use App\Models\RelationType;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RelationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api')){
            return Datatables::of(RelationType::orderBy('order_by','asc')->get())->make(true);
        }
        return view('pages.relation_type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relationtype = false;
        return view('pages.relation_type.create', compact('relationtype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RelationTypeRequest $request)
    {
        try {
            $relationtype = new RelationType;
            $relationtype->relation_name = convert_char_UTF_8($request->name);
            $relationtype->order_by = RelationType::count() + 1;
            $relationtype->save();
            return redirect("relationtype");
//            return redirect()->back()->with('success', 'RelationType Added successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'RelationType not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(RelationType $relationtype)
    {
        return view('pages.relation_type.create', compact('relationtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RelationTypeRequest $request, RelationType $relationtype)
    {
        try {
            $relationtype->relation_name = convert_char_UTF_8($request->name);

            $relationtype->save();
            return redirect("relationtype");
//            return redirect()->back()->with('success', 'RelationType Updated successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'RelationType not Updated...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RelationType $relationtype)
    {
        try {
            $relationtype->delete();
            return response()->json(['data' => 'RelationType deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Relationtype not Deleted...', 'status' => '0']);
        }
    }

    public function change_status($id)
    {
        try {
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $entity = RelationType::where('id', $id)->first();
            $entity->is_active = $value;

            \DB::table('home_screens')
                ->where('relation_type_id', $id)
                ->update(['is_active' => $value]);

            if( $entity->save()){
                return response()->json(['data' => 'category Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'category Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'category Status not changed...', 'status' => '0']);
        }
    }

    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = RelationType::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }
}
