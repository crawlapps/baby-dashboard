<?php

namespace App\Http\Controllers\Relation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Relation\RelationRequest;
use App\Models\HomeScreen;
use App\Models\RelationType;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use App\Models\Relation;
use Yajra\DataTables\DataTables;

class RelationController extends Controller
{
    /**
     * Display a relationtypelisting of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->filled('api')){
//            dd($request);
            $homescreen = HomeScreen::where('relation_type_id', $_GET['id'])->orderBy('order_by','desc')->get();
//                $relation = Relation::where('relation_type_id', $_GET['id'])->orderBy('created_at','desc')->get();
                return Datatables::of($homescreen)->make(true);
        }

//        if($request->filled('api')){
//            return Datatables::of(Relation::query())->make(true);
//        }
        $relation_id = $request->id;
        $relation= RelationType::select('id','relation_name')->where('id',$relation_id)->first();
//        $relation_name = $relation->relation_name;
        return view('pages.relation.index',compact('relation_id','relation'));
    }

    public function homescreen_relation(Request $request){
        if( $request->filled('api')) {
            $relation = Relation::where('home_screen_id', $_GET['home_screen_id'])->orderBy('order_by','asc')->get();
            return Datatables::of($relation)->make(true);
        }
        $home_screen_id = $_GET['home_screen_id'];
        $relation = Relation::select('relation_type_id')->where('home_screen_id', $_GET['home_screen_id'])->first();
        $relation_type_id = $relation->relation_type_id;
        return view('pages.homescreen.relation',compact('home_screen_id','relation_type_id'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $relation = false;
        $relationtype = RelationType::where('id', $request->relation_id )->first();
//        dd($relationtype);
//        $relationtype = RelationType::get();
        return view('pages.relation.create', compact('relation'),compact('relationtype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RelationRequest $request)
    {
        try {
            $relation = new Relation;
            $relation->relation_type_id = $request->relation_type_id;

            if ($request->hasFile('image')) {
                $image = ImageTrait::makeImage($request->image, 'uploads/relation/');
            }
            $relation->image = $image;
            $relation->save();
            return redirect('relation'.'?id='.$relation->relation_type_id);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Relation not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Relation $relation)
    {
        $relationtype = RelationType::where('id', $relation->relation_type_id )->first();
        return view('pages.relation.create', compact('relation'),compact('relationtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RelationRequest $request, Relation $relation)
    {
        try {
            $relation->relation_type_id = $request->relation_type_id;

            if ($request->hasFile('image')) {
                $image = ImageTrait::makeImage($request->image, 'uploads/relation/');
            }else{
                $image = $relation->image;
            }
            $relation->image = $image;
            $relation->save();
            return redirect('relation'.'?id='.$relation->relation_type_id);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Relation not Update...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Relation $relation)
    {
        try {
            $relation->delete();
            return response()->json(['data' => 'Relation deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Relation not Deleted...', 'status' => '0']);
        }
    }

    /**
     * Active /deactive relation image
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function change_status($id)
    {
        try {
            $relation = Relation::where('id', $id)->first();
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $relation->relation_type_id = $relation->relation_type_id;
            $relation->is_enable = $value;
            $relation->image = $relation->image;

            if( $relation->save()){
                return response()->json(['data' => 'Relation Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Relation Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Poster Status not changed...', 'status' => '0']);
        }
    }

    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = Relation::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }
}
