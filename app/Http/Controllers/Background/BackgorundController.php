<?php

namespace App\Http\Controllers\Background;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\Background\BackgroundApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Background\BackgroundRequest;
use App\Models\BackgroundCategory;
use App\Models\Background;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Yajra\DataTables\DataTables;

class BackgorundController extends Controller
{
    use ImageTrait;
    private $cache_key = 'background';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category_id = $request->category_id;

        if($request->filled('api')){
            $background = Background::where('background_category_id',$category_id)->orderBy('order_by','desc')->get();
            return Datatables::of($background)->make(true);
        }

        $category = BackgroundCategory::select('name')->where('id',$category_id)->first();
        $name = $category->name;
        return view('pages.background.index', compact('category_id','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $category = BackgroundCategory::where('id',$request->category_id)->first();
        $background = false;
        return view('pages.background.create',compact('background','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BackgroundRequest $request)
    {
        try {
            $background = new Background;

            $image = ImageTrait::makeImage($request->file_background, 'uploads/background/');

//            // for compress file
//            $this->destinationPath = public_path('/storage/uploads/background/');
//            $this->thumbnailDestinationPath = public_path('/storage/uploads/background/thumbnails/');
//            if (!file_exists( $this->thumbnailDestinationPath )) {
//                mkdir($this->thumbnailDestinationPath, 0777, true);
//            }
//            $this->compress_image($this->destinationPath . $image, $this->thumbnailDestinationPath . $image, env('QUALITY'));

            $background->background_category_id = $request->category;
            $background->file_background = $image;
            $background->order_by = Background::count() + 1;
            $background->save();

//            $this->setCache();
            return redirect('background'.'?category_id='.$background->background_category_id);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Background not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Background $background)
    {
        $category = BackgroundCategory::where('id',$background->background_category_id)->first();
        return view('pages.background.create',compact('background','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BackgroundRequest $request, Background $background)
    {
        try {

            if( $request->file_background ){
                $image = ImageTrait::makeImage($request->file_background, 'uploads/background/');

//                // for compress file
//                $this->destinationPath = public_path('/storage/uploads/background/');
//                $this->thumbnailDestinationPath = public_path('/storage/uploads/background/thumbnails/');
//                if (!file_exists( $this->thumbnailDestinationPath )) {
//                    mkdir($this->thumbnailDestinationPath, 0777, true);
//                }
//                $this->compress_image($this->destinationPath . $image, $this->thumbnailDestinationPath . $image, env('QUALITY'));
            }


            $background->background_category_id = $request->category;
            $background->file_background = $image;
          $background->save();

//            $this->setCache();
            return redirect('background'.'?category_id='.$background->background_category_id);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Background not added...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Background $background)
    {
        try {
            $path = \Storage::path('uploads/background/'.$background->file_background);
            if(file_exists($path)){
                unlink($path);
            }
            $background->delete();

//            $this->setCache();
            return response()->json(['data' => 'Background deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Background not Deleted...', 'status' => '0']);
        }
    }

    public function change_status($id)
    {
        try {
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $entity = Background::where('id', $id)->first();
            $entity->is_active = $value;

//            $this->setCache();
            if( $entity->save()){
                return response()->json(['data' => 'background Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'background Status not changed...', 'status' => '0']);
            }
        } catch (Exception $e) {
            return response()->json(['data' => 'background Status not changed...', 'status' => '0']);
        }
    }

    public function setIsPro(Request $request)
    {
        try {
            $pro = $request->is_pro ? 0 : 1;
            $sticker = Background::where('id', $request->id)->first();
            $sticker->is_pro = $pro;
//            $this->setCache();

            if( $sticker->save()){
                return response()->json(['data' => 'Background Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Background Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Background Status not changed...', 'status' => '0']);
        }
    }

    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = Background::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
//        $this->setCache();
    }

    public function setCache(){
        $api = new BackgroundApiController;
        $entity = $api->index();

        Redis::del(env('REDIS_PREFIX') . $this->cache_key);
        Redis::del($this->cache_key);
        Redis::set($this->cache_key, $entity);
    }
}
