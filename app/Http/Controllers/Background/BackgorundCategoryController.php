<?php

namespace App\Http\Controllers\Background;

use App\Http\Controllers\Api\Background\BackgroundApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Background\BackgroundCategoryRequest;
use App\Models\BackgroundCategory;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Yajra\DataTables\DataTables;

class BackgorundCategoryController extends Controller
{
    use ImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api')){
            return Datatables::of(BackgroundCategory::orderBy('order_by','asc')->get())->make(true);
        }
        return view('pages.background.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $backgroundcategory = false;
        return view('pages.background.category.create', compact('backgroundcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BackgroundCategoryRequest $request)
    {
        try {
            $category = new BackgroundCategory;
            $image = ImageTrait::makeImage($request->file_category, 'uploads/backgroundcategory/');

//            // for compress file
//            $this->destinationPath = public_path('/storage/uploads/backgroundcategory/');
//            $this->thumbnailDestinationPath = public_path('/storage/uploads/backgroundcategory/thumbnails/');
//            if (!file_exists( $this->thumbnailDestinationPath )) {
//                mkdir($this->thumbnailDestinationPath, 0777, true);
//            }
//            $this->compress_image($this->destinationPath . $image, $this->thumbnailDestinationPath . $image, env('QUALITY'));

            $category->name = convert_char_UTF_8($request->name);
            $category->file_background_category = $image;
            $category->order_by = BackgroundCategory::count() + 1;
            $category->save();

//            $this->setCache();
            return redirect('backgroundcategory');
//            return redirect()->back()->with('success', 'Category Added successfully...');


        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Category not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BackgroundCategory $backgroundcategory)
    {
        return view('pages.background.category.create', compact('backgroundcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BackgroundCategoryRequest $request, BackgroundCategory $backgroundcategory)
    {
        try {

            if( $request->file_category ){
                $image = ImageTrait::makeImage($request->file_category, 'uploads/backgroundcategory/');
            }else{
                $image = $backgroundcategory->file_background_category;
            }

            $backgroundcategory->name = convert_char_UTF_8($request->name);
            $backgroundcategory->file_background_category = $image;
            $backgroundcategory->save();

//            $this->setCache();
            return redirect('backgroundcategory');
//            return redirect()->back()->with('success', 'Category Added successfully...');


        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Category not added...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BackgroundCategory $backgroundcategory)
    {
        try {
            $path = \Storage::path('uploads/backgroundcategory/'.$backgroundcategory->file_background_category);
            if(file_exists($path)){
                unlink($path);
            }
            $backgroundcategory->delete();

//            $this->setCache();
            return response()->json(['data' => 'Category deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Category not Deleted...', 'status' => '0']);
        }
    }

    public function change_status($id)
    {
        try {
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $entity = BackgroundCategory::where('id', $id)->first();
            $entity->is_active = $value;

            \DB::table('backgrounds')
                ->where('background_category_id', $id)
                ->update(['is_active' => $value]);

//            $this->setCache();
            if( $entity->save()){
                return response()->json(['data' => 'category Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'category Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'category Status not changed...', 'status' => '0']);
        }
    }

    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = BackgroundCategory::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
//        $this->setCache();
    }

    public function setCache(){
        $back = new BackgorundController();
        $back->setCache();
    }
}
