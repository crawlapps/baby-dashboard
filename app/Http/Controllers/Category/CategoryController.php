<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryRequest;
use App\Models\Category;
use App\Models\MonthlyPoster;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api')){
            return Datatables::of(Category::orderBy('order_by','asc')->get())->make(true);
        }
        return view('pages.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = false;
        return view('pages.category.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        try {
            $category = new Category;
            $image = ImageTrait::makeImage($request->file_category, 'uploads/category/');

//            // for compress file
//            $destinationPath = public_path('/storage/uploads/category/');
//            $thumbnailDestinationPath = public_path('/storage/uploads/category/thumbnails/');
//            if (!file_exists( $thumbnailDestinationPath )) {
//                mkdir($thumbnailDestinationPath, 0777, true);
//            }
//            $this->compress_image($destinationPath . $image, $thumbnailDestinationPath . $image, env('QUALITY'));
            $category->file_category = $image;
            $category->name = convert_char_UTF_8($request->name);
            $category->order_by = Category::count() + 1;

            $category->save();
            return redirect('category');
//            return redirect()->back()->with('success', 'Category Added successfully...');


        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Category not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('pages.category.create', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        try {
            $image = $category->file_category;
            if ($request->hasFile('file_category')) {
                $image = ImageTrait::makeImage($request->file_category, 'uploads/category/');

//                // for compress file
//                $destinationPath = public_path('/storage/uploads/category/');
//                $thumbnailDestinationPath = public_path('/storage/uploads/category/thumbnails/');
//                if (!file_exists( $thumbnailDestinationPath )) {
//                    mkdir($thumbnailDestinationPath, 0777, true);
//                }
//                $this->compress_image($destinationPath . $image, $thumbnailDestinationPath . $image, env('QUALITY'));

                $category->file_category = $image;
            }else{
                $category->file_category = $image;
            }
            $category->name = convert_char_UTF_8($request->name);
            $category->save();

            return redirect('category');
//            return redirect()->back()->with('success', 'Category Updated successfully...');

        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Category not update...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {
            $path = \Storage::path('uploads/category/'.$category->file_category);
            if(file_exists($path)){
                unlink($path);
            }
            $category->delete();
            return response()->json(['data' => 'Category deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Category not Deleted...', 'status' => '0']);
        }
    }

    public function change_lock($id)
    {
        try {
            $category = Category::where('id', $id)->first();
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $category->name = $category->name;
            $category->is_lock = $value;
            if( $category->save()){
                return response()->json(['data' => 'Category Lock changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Category Lock not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Category Lock not changed...', 'status' => '0']);
        }

    }
    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = Category::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }

}
