<?php

namespace App\Http\Controllers\HomeScreen;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomeScreen\HomeScreenRequest;
use App\Http\Requests\Relation\RelationRequest;
use App\Models\CollageCategory;
use App\Models\HomeScreen;
use App\Models\Monthly_Collage;
use App\Models\Relation;
use App\Models\RelationType;
use App\Models\SingleCategory;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class HomeScreenController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->filled('api')) {
            $entity = HomeScreen::where('is_home', 1)->orderBy('updated_at', 'desc')->get();
            return Datatables::of($entity)->make(true);
        }
        return view('pages.homescreen.index');
    }

    public function typeindex($type, Request $request)
    {
        if ($request->filled('api')) {
            $entity = "";
            if ($request->type == "3") {
//                $entity = HomeScreen::with('collage')->where('type', $request->type)->orderBy('created_at','desc')->get();
                $entity = HomeScreen::where('type', $request->type)->orderBy('order_by','desc')->get();
            } else if( $request->type == "2" ){
                $entity = HomeScreen::where('type', $request->type)->orderBy('order_by','desc')->get();
            }else if( $request->type == "1" ){
                $entity = HomeScreen::where('type', $request->type)->orderBy('order_by','desc')->get();
            }
            return Datatables::of($entity)->make(true);
        }

        if ($type == "1") {
            $view = 'single';
        } elseif ($type == "2") {
            $view = 'relation';
        } elseif ($type == "3") {
            $view = 'collages';
        }
        return view('pages.homescreen.' . $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = $_GET['type'];
        $homescreen = false;
        $collages = false;

        $relationtype = ( $_GET['type'] == "2") ? RelationType::where('id',$_GET['relation_type'])->first() : false;
        $singlecategory = ( $_GET['type'] == "1") ? SingleCategory::where('id',$_GET['single_category_id'])->first() : false;
//        $collagecategory = ( $_GET['type'] == "3") ? CollageCategory::where('id',$_GET['collage_category_id'])->first() : false;

        $relation = false;
        return view('pages.homescreen.create', compact('homescreen', 'type', 'collages','relationtype','relation','singlecategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(HomeScreenRequest $request)
    {
        try {
            $homescreen = new HomeScreen;
            $homescreen->type = $request->type;
            $homescreen->title = convert_char_UTF_8($request->title);

            if ($request->type == "1" || $request->type == "2"  ) {
                if( $request->type == "1" ){
                    $homescreen->single_category_id = $request->single_category_id;
                    $homescreen->x = $request->x;
                    $homescreen->y = $request->y;
                    $homescreen->height = $request->height;
                    $homescreen->width = $request->width;
                    $homescreen->rotation = $request->rotation;
                    $homescreen->ratio = preg_replace('/\s+/', '', $request->ratio);
                }

                if ($request->hasFile('file_homescreen')) {
                    $image = ImageTrait::makeImage($request->file_homescreen, 'uploads/homescreens/');
                }

                if ($request->hasFile('file_withouttransparent')) {
                    $image2 = ImageTrait::makeImage($request->file_withouttransparent, 'uploads/homescreens/');
                }
                $homescreen->relation_type_id = $request->relation_type_id;
                $homescreen->file_homescreen = $image;
                if( $request->type == "1" ) {
                    $homescreen->file_withouttransparent = $image2;
                }
                $homescreen->is_home = ( $request->is_home != '' ) ? $request->is_home : 0;
                $homescreen->is_active = 0;
                $homescreen->order_by = HomeScreen::count() + 1;
                $homescreen->save();
            }

            if( $request->type == "2" ){
                if ($request->hasFile('relation')) {
                    foreach ($request->relation as $ik=>$image) {
                        $relation = new Relation;
                        $relation->relation_type_id = $request->relation_type_id;
                        $relation->home_screen_id = $homescreen->id;
                        $image = ImageTrait::makeImage($image, 'uploads/relation/');
                        $relation->image = $image;
                        $relation->order_by = $ik;
                        $relation->is_enable = 1;

                        $relation->save();
                    }
                }
            }

            if ($request->type == "3") {
                $homescreen->grid = $request->grid;
                $image = "";
                if ($request->hasFile('file_grid')) {
                    $image = ImageTrait::makeImage($request->file_grid, 'uploads/grid/');
                }
                $homescreen->file_grid = $image;

//                $position = (is_array($request->collage)) ? 1 : 0;
                $homescreen->is_position = 1;
                $homescreen->ratio = preg_replace('/\s+/', '', $request->ratio);
                $homescreen->is_home = ( $request->is_home != '' ) ? $request->is_home : 0;
                $homescreen->is_active = 0;
                $homescreen->order_by = HomeScreen::count() + 1;
                if ($request->hasFile('file_withoutbaby')) {
                    $image2 = ImageTrait::makeImage($request->file_withoutbaby, 'uploads/homescreens/');
                }
                $homescreen->file_withouttransparent = $image2;
                $homescreen->save();

                if (is_array($request->position)) {
                    foreach ($elements = $request->position as $i => $val) {
                            $collages = new Monthly_Collage;
                            $collages->home_screen_id = $homescreen->id;
                            $collages->position = $i;
                            $collages->x = $elements[$i]['x'];
                            $collages->y = $elements[$i]['y'];
                            $collages->width = $elements[$i]['width'];
                            $collages->height = $elements[$i]['height'];
                            $collages->rotation = $elements[$i]['rotation'];
                            $collages->save();
                    }
                }
            }

            if( $homescreen->type == "1" ){
                return response()->json(['data' => 'Home Screen Added Successfully...', 'redirect' => url('singlehomescreen?id='.$homescreen->single_category_id)], 200);
            }
            if( $homescreen->type == "2" ){
                return response()->json(['data' => 'Home Screen Added Successfully...', 'redirect' => url('relation?id='.$homescreen->relation_type_id )], 200);
            }
            if( $homescreen->type == "3" ){
                return response()->json(['data' => 'Home Screen Added Successfully...', 'redirect' => url('collagehomescreen?id='.$homescreen->collage_category_id)], 200);
            }
        } catch (Exception $e) {
            return response()->json(['data' => 'Home Screen not added...'], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(HomeScreen $homescreen)
    {
        if( $homescreen->type == "3" ){
            $homescreen->_collage = $homescreen->collage()->get();
        }elseif( $homescreen->type == "2" ){
            $homescreen->_relation = $homescreen->relation()->get();
        }elseif( $homescreen->type == "1" ){
            $homescreen = $homescreen;
        }
        $type = $homescreen->type;
        $relationtype = ( $type == "2") ? RelationType::where('id',$homescreen->relation_type_id)->first() : false;
        $singlecategory = ( $type == "1") ? SingleCategory::where('id',$homescreen->single_category_id)->first() : false;
//        $collagecategory = ( $type == "3") ? CollageCategory::where('id',$homescreen->collage_category_id)->first() : false;
        return view('pages.homescreen.create', compact('homescreen', 'type','relationtype','singlecategory'));
    }
    public function collageHomescreen(Request $request){
        if ($request->filled('api')) {
            $entity = HomeScreen::with('collage')->where('type', '3')->orderBy('order_by', 'desc')->get();
            return Datatables::of($entity)->make(true);
        }


        return view('pages.homescreen.collages');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(HomeScreenRequest $request, HomeScreen $homescreen)
    {

        try {
            $image = "";
            if ($request->type == "1" || $request->type == "2" ) {
                if( $request->type == "1" ){
                    $homescreen->single_category_id = $request->single_category_id;
                    $homescreen->x = $request->x;
                    $homescreen->y = $request->y;
                    $homescreen->height = $request->height;
                    $homescreen->width = $request->width;
                    $homescreen->rotation = $request->rotation;
                    $homescreen->ratio = preg_replace('/\s+/', '', $request->ratio);
                }

                if ($request->hasFile('file_homescreen')) {
                    $image = ImageTrait::makeImage($request->file_homescreen, 'uploads/homescreens/');

                } else {
                    $image = $homescreen->file_homescreen;
                }
                if ($request->hasFile('file_withouttransparent')) {
                    $image2 = ImageTrait::makeImage($request->file_withouttransparent, 'uploads/homescreens/');
                }else{
                    $image2 = $homescreen->file_withouttransparent;
                }

                $homescreen->relation_type_id = $request->relation_type_id;
                $homescreen->title = convert_char_UTF_8($request->title);
                $homescreen->type = $request->type;
                $homescreen->file_homescreen = $image;
                if( $request->type == "1" ){
                    $homescreen->file_withouttransparent = $image2;
                }

                $homescreen->is_home = ( $request->is_home != '' ) ? $request->is_home : 0;
                $homescreen->save();
            }

            if( $request->type == "2" ){
                $relation = $homescreen->relation()->get();
                if ($request->hasFile('relation')) {
                    \DB::table('relations')->where('home_screen_id', $homescreen->id)->delete();
                    foreach ($request->relation as $image) {
                        $relation = new Relation;
                        $relation->relation_type_id = $request->relation_type_id;
                        $relation->home_screen_id = $homescreen->id;
                        $image = ImageTrait::makeImage($image, 'uploads/relation/');
                        $relation->image = $image;
                        $relation->is_enable = 1;

                        $relation->save();
                    }
                }
            }


            if ($request->type == "3") {
                $collage = $homescreen->collage()->get();
                $homescreen->grid = $request->grid;
                $image = "";
                if ($request->hasFile('file_grid')) {
                    $image = ImageTrait::makeImage($request->file_grid, 'uploads/grid/');
                } else {
                    $image = $homescreen->file_grid;
                }
                $homescreen->file_grid = $image;
                $position = (is_array($request->collage)) ? 1 : 0;
                $homescreen->is_position = $position;
                $homescreen->title = $request->title;
                $homescreen->ratio = preg_replace('/\s+/', '', $request->ratio);
                $homescreen->is_home = ( $request->is_home != '' ) ? $request->is_home : 0;

                if ($request->hasFile('file_withoutbaby')) {
                    $image2 = ImageTrait::makeImage($request->file_withoutbaby, 'uploads/homescreens/');
                }else{
                    $image2 = $homescreen->file_withouttransparent;
                }
                $homescreen->file_withouttransparent = $image2;
                $homescreen->save();

                \DB::table('monthly_collages')->where('home_screen_id', $homescreen->id)->delete();
                if (is_array($request->position)) {
                    foreach ($elements = $request->position as $i => $val) {
                        $collages = new Monthly_Collage;
                        $collages->home_screen_id = $homescreen->id;
                        $collages->position = $i;
                        $collages->x = $elements[$i]['x'];
                        $collages->y = $elements[$i]['y'];
                        $collages->width = $elements[$i]['width'];
                        $collages->height = $elements[$i]['height'];
                        $collages->rotation = $elements[$i]['rotation'];
                        $collages->save();
                    }
                }

            }
            if ($homescreen->type == "1") {
                $view = 'single';
            } elseif ($homescreen->type == "2") {
                $view = 'relation';
            } else {
                $view = 'collages';
            }

            if( $homescreen->type == "1" ){
                return response()->json(['data' => 'Home Screen Added Successfully...', 'redirect' => url('singlehomescreen?id='.$homescreen->single_category_id)], 200);
            }
            if( $homescreen->type == "2" ){
                return response()->json(['data' => 'Home Screen Updated Successfully...', 'redirect' => url('relation?id='.$homescreen->relation_type_id)], 200);
            }
            if( $homescreen->type == "3" ){
                return response()->json(['data' => 'Home Screen Added Successfully...', 'redirect' => url('collagehomescreen?id='.$homescreen->collage_category_id)], 200);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Home Screen Not Updated...'], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(HomeScreen $homescreen)
    {
        try {
            if( $homescreen->file_homescreen ){
                $path = \Storage::path('uploads/homescreens/'.$homescreen->file_homescreen);
                if(file_exists($path)){
                    unlink($path);
                }
            }

            if( $homescreen->file_grid ) {
                $path = \Storage::path('uploads/grid/' . $homescreen->file_grid);
                if (file_exists($path)) {
                    unlink($path);
                }
            }
            if( $homescreen->file_withouttransparent ) {
                $path = \Storage::path('uploads/homescreens/' . $homescreen->file_withouttransparent);
                if (file_exists($path)) {
                    unlink($path);
                }
            }
            $homescreen->delete();
            return response()->json(['data' => 'Home Screen deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Home Screen not Deleted...', 'status' => '0']);
        }
    }

    /**
     * change the status of homescreen.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function change_status($id)
    {
        try {
            $homescreen = HomeScreen::where('id', $id)->first();
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $homescreen->is_home = $value;
            ( $value ) ? $homescreen->is_pro = 0 : '';
            if( $homescreen->save()){
                return response()->json(['data' => 'Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Status not changed...', 'status' => '0']);
        }
    }

    public function change_activestatus($id)
    {
        try {
            $homescreen = HomeScreen::where('id', $id)->first();
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $homescreen->is_active = $value;
            if( $homescreen->save()){
                return response()->json(['data' => 'Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Status not changed...', 'status' => '0']);
        }
    }

    public function setIsPro(Request $request)
    {
        try {
            $pro = $request->is_pro ? 0 : 1;
            $sticker = HomeScreen::where('id', $request->id)->first();
            $sticker->is_pro = $pro;
            if( $sticker->save()){
                return response()->json(['data' => 'homescreen Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'homescreen Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'homescreen Status not changed...', 'status' => '0']);
        }
    }

    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = HomeScreen::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }
}
