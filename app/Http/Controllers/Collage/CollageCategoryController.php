<?php

namespace App\Http\Controllers\Collage;

use App\Http\Controllers\Controller;
use App\Http\Requests\Collage\CollageCategoryRequest;
use App\Models\CollageCategory;
use App\Models\HomeScreen;
use App\Models\Monthly_Collage;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CollageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api')){
            return Datatables::of(CollageCategory::orderBy('order_by','desc')->get())->make(true);
        }
        return view('pages.collage.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collagecategory = false;
        return view('pages.collage.create', compact('collagecategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CollageCategoryRequest $request)
    {
        try {
            $category = new CollageCategory;
            $category->name = convert_char_UTF_8($request->name);
            $category->order_by = CollageCategory::count() + 1;
            $category->save();
            return redirect("collagecategory");
//            return redirect()->back()->with('success', 'RelationType Added successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'category not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CollageCategory $collagecategory)
    {
        return view('pages.collage.create', compact('collagecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CollageCategoryRequest $request, CollageCategory $collagecategory)
    {
        try {
            $collagecategory->name = convert_char_UTF_8($request->name);

            $collagecategory->save();
            return redirect("collagecategory");
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'category not added...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CollageCategory $collagecategory)
    {
        try {
            $collagecategory->delete();
            return response()->json(['data' => 'Category deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Category not Deleted...', 'status' => '0']);
        }
    }
    // redirect single category to collage index balde

    public function collageHomescreen(Request $request){
        $category_id = $request->id;
        if ($request->filled('api')) {
            $entity = HomeScreen::with('collage')->where('type', '3')->where('collage_category_id', $category_id)->orderBy('order_by', 'asc')->get();
            return Datatables::of($entity)->make(true);
        }

        $category= CollageCategory::select('id','name')->where('id',$category_id)->first();

        return view('pages.homescreen.collages',compact('category_id','category'));
    }

    public function change_status($id)
    {
        try {
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $entity = CollageCategory::where('id', $id)->first();
            $entity->is_active = $value;

            \DB::table('home_screens')
                ->where('collage_category_id', $id)
                ->update(['is_active' => $value]);

            if( $entity->save()){
                return response()->json(['data' => 'category Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'category Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'category Status not changed...', 'status' => '0']);
        }
    }

    public function collageElements(Request $request){
        $homescreen_id = $request->id;
        if ($request->filled('api')) {
            $entity = Monthly_Collage::where('home_screen_id', $homescreen_id)->orderBy('position', 'asc')->get();
            return Datatables::of($entity)->make(true);
        }
        $homescreen = HomeScreen::select('collage_category_id')->where('id', $homescreen_id)->first();
        $collage_category = $homescreen->collage_category_id;

        return view('pages.homescreen.collage-element',compact('collage_category'));
    }
    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = Monthly_Collage::find($val);
            $entity->position = $key;
            $entity->save();
        }
    }

    public function categoryreOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = CollageCategory::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }
}
