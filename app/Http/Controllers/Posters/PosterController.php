<?php

namespace App\Http\Controllers\Posters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Posters\PosterRequest;
use App\Models\MonthlyPoster;
use App\Models\Relation;
use App\Traits\ImageTrait;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PosterController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api')){
            $entity = MonthlyPoster::where('month_id',$_GET['month'])->orderBy('order_by','desc')->get();
            return Datatables::of($entity)->make(true);
        }
        $month = $request->month;

        return view('pages.poster.index',compact('month'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $poster = false;
        $month_int = $request->month;
        return view('pages.poster.create', compact('poster'),compact('month_int'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PosterRequest $request)
    {
        try {
                $image = ImageTrait::makeImage($request->poster, 'uploads/poster/');
                $image2 = ImageTrait::makeImage($request->file_posterwithbaby, 'uploads/poster/');
                $poster = new MonthlyPoster;

                $poster->month_id = $request->month;
                $poster->poster = $image;
                $poster->file_posterwithbaby = $image2;
                $poster->order_by = MonthlyPoster::count() + 1;
                $poster->save();

                return redirect('poster'.'?month='.$poster->month_id);
//                return redirect()->back()->with('success', 'Poster Added successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Poster not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MonthlyPoster $poster)
    {
        $month_int = $poster->month->month;
        return view('pages.poster.create', compact('poster'),compact('month_int'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PosterRequest $request,MonthlyPoster $poster)
    {
        try {
            if ($request->hasFile('poster')) {
                    $image = ImageTrait::makeImage($request->poster, 'uploads/poster/');
            }else{
                $image = $poster->poster;
            }

            if ($request->hasFile('file_posterwithbaby')) {
                $image2 = ImageTrait::makeImage($request->file_posterwithbaby, 'uploads/poster/');
            }else{
                $image2 = $poster->file_posterwithbaby;
            }

            $poster->month_id = $request->month;
            $poster->poster = $image;
            $poster->file_posterwithbaby = $image2;
            $poster->save();
            return redirect('poster'.'?month='.$poster->month_id);
//            return redirect()->back()->with('success', 'Poster Updated successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Poster not Update...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyPoster $poster)
    {
        try {
            $path = \Storage::path('uploads/poster/'.$poster->poster);
            if(file_exists($path)){
                unlink($path);
            }
            $path = \Storage::path('uploads/poster/'.$poster->file_posterwithbaby);
            if(file_exists($path)){
                unlink($path);
            }
            $poster->delete();
            return response()->json(['data' => 'Poster deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Poster not Deleted...', 'status' => '0']);
        }
    }

    /**
     * change the status og poster.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function change_status($id)
    {
        try {
            $poster = MonthlyPoster::where('id', $id)->first();
            $value = ( $_POST['data'] && $_POST['data'] == "1" ) ? 0 : 1;
            $poster->month_id = $poster->month_id;
            $poster->poster = $poster->poster;
            $poster->is_enable = $value;
            if( $poster->save()){
                return response()->json(['data' => 'Poster Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Poster Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Poster Status not changed...', 'status' => '0']);
        }
    }

    /**
     * milestone index
     */
        public function milestone_index(){

            return view('pages.milestone.index');
        }

    public function setIsPro(Request $request)
    {
        try {
            $pro = $request->is_pro ? 0 : 1;
            $sticker = MonthlyPoster::where('id', $request->id)->first();
            $sticker->is_pro = $pro;
            if( $sticker->save()){
                return response()->json(['data' => 'Poster Status changed...', 'status' => '1']);
            }else{
                return response()->json(['data' => 'Poster Status not changed...', 'status' => '0']);
            }

        } catch (Exception $e) {
            return response()->json(['data' => 'Poster Status not changed...', 'status' => '0']);
        }
    }

    public function reOrder(Request $request)
    {
        $request->items = array_filter($request->items);
        foreach ($request->items as $key => $val) {
            $entity = MonthlyPoster::find($val);
            $entity->order_by = $key;
            $entity->save();
        }
    }
}
