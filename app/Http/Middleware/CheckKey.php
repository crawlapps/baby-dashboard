<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;

class CheckKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $k = base64_decode($request->header('Authorization_key'));
        $key = Setting::where('key', $k)->first();
        if( $key ){
            return $next($request);
        }else{
            return response()->json([
                "message" => "Unauthorized user",
            ], 401);
        }
    }
}
